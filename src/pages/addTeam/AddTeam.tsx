import React, {FC, useCallback, useState} from 'react';
import { useForm } from "react-hook-form";
import { INewTeamDTO } from "../../api/dto/ITeam";
import {useHistory} from "react-router-dom";
import {TeamForm} from "../../ui/teamForm/TeamForm";
import {useAppDispatch} from "../../core/redux/hooks";
import {addNewTeam} from "../../modules/teams/teamsThunk";

export interface TeamFormData extends INewTeamDTO {}

export const AddTeam: FC = () => {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const {
        handleSubmit,
        control,
        formState: { errors }
    } = useForm<TeamFormData>();
    const [imageFile, setImageFile] = useState<File>();
    const getImageFile = useCallback( (imageFile: File) => setImageFile(imageFile), []);
    const onSuccessAddition = useCallback( () => history.push('/teams'), [history]);
    const onSubmit = (data: TeamFormData) => {
        dispatch(addNewTeam( {
            data,
            imageFile,
            onSuccess: onSuccessAddition,
        }));
    };
    const onClickCancel = useCallback( () => history.push('/teams'), [history]);
    return (
        <TeamForm
            errors={errors}
            control={control}
            onSubmit={handleSubmit(onSubmit)}
            onClickCancel={onClickCancel}
            getImageFile={getImageFile}
        />
    );
};
