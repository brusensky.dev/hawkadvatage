import React, {FC, ReactChild, ReactNode} from 'react';
import { Route, Redirect } from 'react-router-dom'

interface IRouteProps {
    exact?: boolean,
    path: string,
    children: ReactChild | ReactNode,
}

export const PrivateRoute: FC<IRouteProps> = (props) => {
    const authToken = localStorage.getItem('token');
    return (
        <Route {...props}>
            { authToken
                ? props.children
                : <Redirect to='/auth/sign-in'/>
            }
        </Route>
    );
};
