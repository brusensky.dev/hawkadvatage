import React, {FC, useEffect} from 'react';
import { Empty } from "../../../ui/empty/Empty";
import picture from '../../../assets/images/playersE_pic.svg';
import styled from "styled-components";
import { PlayerCard } from "../../../ui/playerCard/PlayerCard";
import { IPlayer } from "../../../api/dto/IPlayer";
import {useAppDispatch} from "../../../core/redux/hooks";
import { fetchTeams } from "../../../modules/teams/teamsThunk";
import { getTeamName } from "../../utils";
import {selectTeams, selectTeamsIsLoaded} from "../../../modules/teams/teamsSelector";
import {useSelector} from "react-redux";

interface IProps {
    players: IPlayer[];
}

export const Players: FC<IProps> = ({players}) => {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(fetchTeams({}));
    }, [dispatch]);
    const teamsIsLoaded = useSelector(selectTeamsIsLoaded);
    const teams = useSelector(selectTeams);
    return (
        <>
            { !teamsIsLoaded
                ? <Loading>Loading...</Loading>
                : <StyledContainer>
                    <ContentBlock>
                        { players.length ? players.map((player) =>
                            <PlayerCard
                                name={player.name}
                                number={player.number}
                                team={getTeamName(player.team, teams)}
                                imageUrl={player.avatarUrl || ''}
                                id={player.id}
                                key={player.id}
                            />) : null
                        }
                        { !players.length && <Empty picture={picture}>Add new players to continue</Empty>}
                  </ContentBlock>
                </StyledContainer>
            }
        </>
    );
};
const Loading = styled.h1`
  display: flex;
  height: 100%;
  @media (${ props => props.theme.media.desktop }) {
    padding: 32px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 16px 12px;
  }
`;
const StyledContainer = styled.div`
    height: 100%;
`;
const ContentBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
  overflow: auto;
  @media (${ props => props.theme.media.desktop }) {
    padding: 32px 0;
  }
  @media (${ props => props.theme.media.large }) {
    justify-content: space-between;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 16px 12px;
    justify-content: space-between;
  }
`;

