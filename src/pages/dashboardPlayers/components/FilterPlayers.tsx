import React, {BaseSyntheticEvent, FC, useCallback} from 'react';
import { Button } from "../../../ui/button/Button";
import styled from "styled-components";
import { Search } from '../../../ui/search/Search';
import { useHistory } from 'react-router-dom';
import Select, { OptionsType } from "react-select";
import { theme as styledTheme } from "../../../assets/theme/theme";
import {selectTeamNames} from "../../../modules/teams/teamsSelector";
import {useSelector} from "react-redux";

interface IFilterProps {
    setName: (name: string) => void,
    setTeamIds: (teamsIds: number[]) => void,
}
type MyOption = {
    label: string,
    value: string
}

export const FilterPlayers: FC<IFilterProps> = ({setName, setTeamIds}) => {
    const history = useHistory();
    const onClick = useCallback( () => history.push(`${history.location.pathname}/add`), [history]);
    const onChange = useCallback( (e: BaseSyntheticEvent) => setName(e.target.value), [setName]);
    const handleChangeSelect = useCallback( ( selectedOptions: OptionsType<MyOption> ) => {
        setTeamIds(selectedOptions.map( option => +option.value ));
    }, [setTeamIds]);
    const teams = useSelector(selectTeamNames);
    return (
        <Block>
            <Block1>
                <Search
                    onChange={onChange}
                    margin='auto 0 24px 0'
                />
                <CustomSelect
                    options={teams}
                    isMulti
                    classNamePrefix='Select'
                    onChange={handleChangeSelect}
                    theme = {(theme: any) => ({
                        ...theme,
                        borderRadius: 0,
                        colors: {
                            ...theme.colors,
                            primary25: `${ styledTheme.colors.lightestPrimary1 }`,
                            primary50: `${ styledTheme.colors.lightestAccent }`,
                            primary: `${ styledTheme.colors.darkAccent }`,
                            neutral10: `${ styledTheme.colors.accent }`,
                        },
                    })}
                />
            </Block1>
            <Button onClick={onClick}>Add +</Button>
        </Block>
    );
};
const CustomSelect = styled(Select)`
  & .Select__control {
    height: 40px;
    border-radius: 4px;
    border: 0.5px solid ${ styledTheme.colors.lightestPrimary };
    background: ${ styledTheme.colors.basic };
    width: 366px;
      @media (${ styledTheme.media.phone }) {
        width: 100%;
      }
  }
  & .Select__singleValue {
    color: ${ styledTheme.colors.darkPrimary };
  }
  & .Select__multi-value {
    .Select__multi-value__label {
      color: ${ styledTheme.colors.basic };
    }
    border-radius: 4px;
    color: ${ styledTheme.colors.basic };
  }
`;
const Block = styled.div`
  display: flex;
  @media (${ props => props.theme.media.desktop }) {
    justify-content: space-between;
    height: 40px;
    width: 1140px;
  }
  @media (${ props => props.theme.media.large }) {
    height: auto;
    width: 752px;
  }
  @media (${ props => props.theme.media.phone }) {
    flex-direction: column;
    padding: 0 12px;
    width: 100%;
  }
`;
const Block1 = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    width: 752px;
    display: flex;
    justify-content: space-between;
    height: 40px;
  }
  @media (${ props => props.theme.media.large }) {
    height: auto;
    flex-direction: column;
    margin-right: 24px;
  }
  @media (${ props => props.theme.media.medium }) {
    width: auto;
    flex-direction: column;
    align-items: center;
  }
  @media (${ props => props.theme.media.phone }) {
    margin-bottom: 16px;
    margin-right: 0;
  }
`;
