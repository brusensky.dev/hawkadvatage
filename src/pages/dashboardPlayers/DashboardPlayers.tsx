import React, {FC, useEffect, useState} from 'react';
import styled from "styled-components";
import { Pagination } from '../../ui/pagination/Pagination';
import { FilterPlayers } from "./components/FilterPlayers";
import { Players } from "./components/Players";
import { useAppDispatch } from "../../core/redux/hooks";
import {fetchPlayers} from "../../modules/players/playersThunk";
import {useSelector} from "react-redux";
import {selectNumberOfPlayers, selectPlayers} from "../../modules/players/playersSelector";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  @media (${ props => props.theme.media.phone }) {
    max-width: 414px;
  }
`;

export const DashboardPlayers: FC = () => {
    const dispatch = useAppDispatch();
    const playersList = useSelector(selectPlayers);
    const playersCount = useSelector(selectNumberOfPlayers);
    const [pageNumber, setPageNumber] = useState(1);
    const [pageSize, setPageSize] = useState(6);
    const [playerName, setPlayerName] = useState('');
    const [teamIds, setTeamIds] = useState([] as number[]);
    useEffect(() => {
        dispatch( fetchPlayers({
            name: playerName,
            teamIds: teamIds,
            page: pageNumber,
            pageSize: pageSize
        }));
    },[ playerName, pageNumber, pageSize, teamIds, dispatch ]);
    return (
        <Container>
            <FilterPlayers
                setName={setPlayerName}
                setTeamIds={setTeamIds}
            />
            <Players players={ playersList }/>
            <Pagination
                itemsCount={playersCount}
                pageSize={pageSize}
                setPage={setPageNumber}
                setPageSize={setPageSize}
            />
        </Container>
    );
};
