import React, {FC} from 'react';
import { Empty } from "../../../ui/empty/Empty";
import picture from "../../../assets/images/teamsE_pic.svg";
import styled from "styled-components";
import { TeamCard } from "../../../ui/teamCard/TeamCard";
import { ITeam } from "../../../api/dto/ITeam";
import {useSelector} from "react-redux";
import {selectTeamsIsLoaded} from "../../../modules/teams/teamsSelector";

interface IProps {
    teams: ITeam[];
}

export const Teams: FC<IProps> = ({teams}) => {
    const teamsIsLoaded = useSelector(selectTeamsIsLoaded);
    return (
        <>
            { !teamsIsLoaded
                ? <Loading>Loading...</Loading>
                : <StyledContainer>
                    <ContentBlock>
                        { teams.length
                            ? teams.map((team) =>
                                <TeamCard
                                    id={team.id}
                                    name={team.name}
                                    foundationYear={team.foundationYear}
                                    imageUrl={team.imageUrl || ''}
                                    key={team.id}
                                /> )
                            : null
                        }
                        { !teams.length && <Empty picture={picture}>Add new teams to continue</Empty>}
                    </ContentBlock>
                </StyledContainer>
            }
        </>
    );
};
const Loading = styled.h1`
  display: flex;
  height: 100%;
  @media (${ props => props.theme.media.desktop }) {
    padding: 32px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 16px 12px;
  }
`;
const StyledContainer = styled.div`
    height: 100%;
`;
const ContentBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
  overflow: auto;
  @media (${ props => props.theme.media.desktop }) {
    padding: 32px 0;
  }
  @media (${ props => props.theme.media.large }) {
    justify-content: space-between;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 16px 12px;
    justify-content: space-between;
  }
`;
