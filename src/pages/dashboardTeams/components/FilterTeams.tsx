import React, {BaseSyntheticEvent, FC, useCallback} from 'react';
import { Button } from "../../../ui/button/Button";
import styled from "styled-components";
import { Search } from '../../../ui/search/Search';
import { useHistory } from 'react-router-dom';

interface IFilterProps {
    setName: (name: string) => void,
}

export const FilterTeams: FC<IFilterProps> = ({setName}) => {
    const history = useHistory();
    const onClick = useCallback( () => history.push(`${history.location.pathname}/add`), [history]);
    const onChange = useCallback( (e: BaseSyntheticEvent) => setName(e.target.value), [setName]);
    return (
        <StyledContainer>
            <StyledInputContainer>
                <Search
                    onChange={onChange}
                    margin='auto 24px auto 0'
                />
            </StyledInputContainer>
            <Button onClick={onClick}>Add +</Button>
        </StyledContainer>
    );
};
const StyledContainer = styled.div`
  display: flex;
  @media (${ props => props.theme.media.desktop }) {
    width: 1140px;
    justify-content: space-between;
    height: 40px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 752px;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 0 12px;
    width: 100%;
    flex-direction: column;
  }
`;
const StyledInputContainer = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    display: flex;
    justify-content: space-between;
    height: 40px;
  }
`;
