import React, {FC, useEffect, useState} from 'react';
import styled from "styled-components";
import { Pagination } from '../../ui/pagination/Pagination';
import { FilterTeams } from "./components/FilterTeams";
import { Teams } from "./components/Teams";
import { useAppDispatch } from "../../core/redux/hooks";
import { fetchTeams } from "../../modules/teams/teamsThunk";
import {useSelector} from "react-redux";
import {selectNumberOfTeams, selectTeams} from "../../modules/teams/teamsSelector";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  @media (${ props => props.theme.media.phone }) {
    max-width: 414px;
  }
`;

export const DashboardTeams: FC = () => {
    const dispatch = useAppDispatch();
    const teamsList = useSelector(selectTeams);
    const teamsCount = useSelector(selectNumberOfTeams);
    const [pageNumber, setPageNumber] = useState(1);
    const [pageSize, setPageSize] = useState(6);
    const [teamName, setTeamName] = useState('');
    useEffect(() => {
        dispatch(fetchTeams({
            name: teamName,
            page: pageNumber,
            pageSize: pageSize
        }));
    },[ teamName, pageNumber, pageSize, dispatch ]);
    return (
        <Container>
            <FilterTeams setName={setTeamName}/>
            <Teams teams={teamsList}/>
            <Pagination
                itemsCount={teamsCount}
                pageSize={pageSize}
                setPage={setPageNumber}
                setPageSize={setPageSize}
            />
        </Container>
    );
};
