import React, {FC, useEffect} from 'react';
import { useParams } from "react-router-dom";
import { useAppDispatch } from "../../core/redux/hooks";
import { PlayerDetailCard } from "../../ui/playerCard/PlayerDetailCard";
import { fetchPlayerById } from "../../modules/players/playersThunk";
import {selectPlayerInfo, selectPlayerInfoIsLoaded} from "../../modules/players/playersSelector";
import {useSelector} from "react-redux";


export const DetailPlayer: FC = () => {
    const dispatch = useAppDispatch();
    const id = Number(useParams<{ id: string }>().id);
    useEffect( () => {
       dispatch(fetchPlayerById(id));
    }, [dispatch, id]);
    const player = useSelector(selectPlayerInfo);
    const isLoaded = useSelector(selectPlayerInfoIsLoaded);
    return (
        <>
            { isLoaded
                ? <PlayerDetailCard player={player}/>
                : <h1>Loading...</h1>
            }
        </>
    );
};
