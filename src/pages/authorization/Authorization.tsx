import React, {FC} from 'react';
import styled from 'styled-components';
import signUpPic from '../../assets/images/signUp_pic.svg';
import signInPic from '../../assets/images/signIn_pic.svg';
import { SignIn } from "./components/signIn/SignIn";
import { SignUp } from './components/signUp/SignUp';
import { Notification } from "../../ui/notification/Notification";
import { useSelector } from "react-redux";
import { selectErrorStatus } from "../../modules/authorization/authorizationSelector";

interface IProps {
    signIn?: boolean,
}

const showNotification = (errorStatus: number|null) => {
    try {
        switch (errorStatus) {
            case 0:
                return null;
            case 401:
                return <Notification>User with the specified username / password was not found.</Notification>;
            case 409:
                return <Notification>User with this already exist.</Notification>;
            default:
                throw Error('Unexpected status code');
        }
    }
    catch ( error ) {
        console.log(error);
    }
};

export const Authorization: FC<IProps> = (props) => {
    const errorStatus = useSelector(selectErrorStatus);
    return (
        <LayoutAuthorization>
            <FormBlock>
                {props.signIn ? <SignIn/> : <SignUp/>}
            </FormBlock>
            <PictureBlock className='image'>
                {props.signIn ? <Picture src={signInPic} /> : <Picture src={signUpPic} />}
            </PictureBlock>
            { showNotification( errorStatus ) }
        </LayoutAuthorization>
    );
};

const LayoutAuthorization = styled.div`
    height: 100vh;
    display: flex;
`;
const FormBlock = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  @media (${ props => props.theme.media.desktop }) {
    width: 48%;
  }
  @media (${ props => props.theme.media.large }) {
    width: 100%;
  }
`;
const PictureBlock = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    width: 52%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #F5FBFF;
  }
  @media (${ props => props.theme.media.large }) {
    display: none;
  }
`;
const Picture = styled.img``;
