import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import { Controller, useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import { Input } from '../../../../ui/input/Input';
import { Button } from "../../../../ui/button/Button";
import { Checkbox } from "../../../../ui/checkbox/Checkbox";
import { InputPassword } from "../../../../ui/input/InputPassword";
import { useAppDispatch } from '../../../../core/redux/hooks';
import { authSignUp } from "../../../../modules/authorization/autorizationThunk";

type signUpFormData = {
    userName: string;
    login: string;
    password: string;
    repeatPassword: string;
    terms: boolean;
};

export const SignUp: FC = () => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const {
        control,
        handleSubmit,
    } = useForm<signUpFormData>();
    const onSuccess = useCallback( () => history.push('/'), [history]);
    const onSubmit = (data: signUpFormData) => dispatch(authSignUp(data))
        .then(onSuccess);
    return (
        <StyledSignUpBlock>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <HeaderContainer>
                    <StyledHeader>Sign Up</StyledHeader>
                </HeaderContainer>
                <Controller
                    control={control}
                    name={'userName'}
                    rules={ {required: 'Required'} }
                    render={ ({field: {value, onChange, name}}) => (
                        <Input
                            label='Name'
                            id={name}
                            type='text'
                            defaultValue={value}
                            onChange={onChange}
                            margin='24px 0 0 0'
                        />
                    )}
                />
                <Controller
                    control={control}
                    name={'login'}
                    rules={ {required: 'Required'} }
                    render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                        <Input
                            label='Login'
                            id={name}
                            type='text'
                            defaultValue={value}
                            onChange={onChange}
                            margin='24px 0 0 0'
                            errorMessage={error?.message}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name={'password'}
                    rules={ {required: 'Required'} }
                    render={ ({field: {onChange, name}, fieldState: {error}}) => (
                        <InputPassword
                            label='Password'
                            id={name}
                            onChange={onChange}
                            errorMessage={error?.message}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name={'repeatPassword'}
                    rules={ {required: 'Required'} }
                    render={ ({field: {onChange, name}, fieldState: {error}}) => (
                        <InputPassword
                            label='Enter your password again'
                            id={name}
                            onChange={onChange}
                            errorMessage={error?.message}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name={'terms'}
                    rules={ {required: 'You must be accept the agreement.'} }
                    render={ ({field: {onChange, name}, fieldState: {error}}) => (
                        <Checkbox
                            label='I accept the agreement'
                            id={name}
                            onChange={onChange}
                            errorMessage={error?.message}
                        />
                    )}
                />
                <Button type='submit'>Sign Up</Button>
                <CheckForMember>
                    Already a member?&nbsp;
                    <Link to='/auth/sign-in'><StyledLink>Sign In</StyledLink></Link>
                </CheckForMember>
            </Form>
        </StyledSignUpBlock>
    );
};
const HeaderContainer = styled.div`
  display: flex;
  @media (${ props => props.theme.media.Large }) {
    justify-content: center;
  }
  margin-bottom: 32px;
`;
const StyledHeader = styled.h1`
  color: ${ props => props.theme.colors.blue };
  font-size: 36px;
  line-height: 49px;
`;
const StyledSignUpBlock = styled.div`
    display: flex;
    width: 48%;
    justify-content: center;
    align-items: center;
`;
const Form = styled.form`
  & button {
    width: 365px;
    margin-top: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    & button {
      width: 327px;
    }
  }
`;
const StyledLink = styled.span`
    color: ${ props => props.theme.colors.accent };
    cursor: pointer;
    text-decoration: underline;
`;
const CheckForMember = styled.div`
    margin-top: 24px;
    font-size: 14px;
    line-height: 24px;
    color: ${ props => props.theme.colors.primary };;
    display: flex;
    justify-content: center;
`;
