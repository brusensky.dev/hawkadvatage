import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import { Input } from '../../../../ui/input/Input';
import { Button } from "../../../../ui/button/Button";
import { Controller, useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import { InputPassword } from "../../../../ui/input/InputPassword";
import { useAppDispatch } from "../../../../core/redux/hooks";
import { authSignIn } from "../../../../modules/authorization/autorizationThunk";

type signInFormData = {
    login: string;
    password: string;
};

export const SignIn: FC = () => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const {
        handleSubmit,
        control,
    } = useForm<signInFormData>();
    const onSuccess = useCallback( () => history.push('/'), [history]);
    const onSubmit = (data: signInFormData) => dispatch(authSignIn(data))
        .then(onSuccess);
    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <HeaderContainer>
                <StyledHeader>Sign In</StyledHeader>
            </HeaderContainer>
            <Controller
                control={control}
                name={'login'}
                rules={ {required: 'Required'} }
                render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                    <Input
                        label='Login'
                        id={name}
                        type='text'
                        defaultValue={value}
                        onChange={onChange}
                        errorMessage={error?.message}
                    />
                )}
            />
            <Controller
                control={control}
                name={'password'}
                rules={ {required: 'Required'} }
                render={ ({field: {onChange, name}, fieldState: {error}}) => (
                    <InputPassword
                        label='Password'
                        id={name}
                        onChange={onChange}
                        errorMessage={error?.message}
                    />
                )}
            />
            <Button type='submit'>Sign In</Button>
            <InvitationToMember>
                Not a member yet?&nbsp;
                <Link to='/auth/sign-up'><StyledLink>Sign Up</StyledLink></Link>
            </InvitationToMember>
        </Form>
    );
};
const HeaderContainer = styled.div`
  display: flex;
  @media (${ props => props.theme.media.large }) {
    justify-content: center;
  }
  margin-bottom: 32px;
`;
const StyledHeader = styled.h1`
  color: ${ props => props.theme.colors.blue };
  font-size: 36px;
  line-height: 49px;
`;
const Form = styled.form`
  & button {
    width: 365px;
    margin-top: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    & button {
      width: 327px;
    }
  }
`;
const StyledLink = styled.span`
    color: ${ props => props.theme.colors.accent };
    cursor: pointer;
    text-decoration: underline;
`;
const InvitationToMember = styled.div`
    margin-top: 24px;
    font-size: 14px;
    line-height: 24px;
    color: ${ props => props.theme.colors.primary };;
    display: flex;
    justify-content: center;
`;
