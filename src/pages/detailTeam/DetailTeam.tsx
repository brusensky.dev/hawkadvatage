import React, {useEffect} from 'react';
import { TeamDetailCard } from "../../ui/teamCard/TeamDetailCard";
import { useAppDispatch } from "../../core/redux/hooks";
import { useParams } from "react-router-dom";
import { Table } from "../../ui/table/Table";
import { fetchTeamById } from "../../modules/teams/teamsThunk";
import { fetchPlayersByTeamId } from "../../modules/players/playersThunk";
import {selectTeamInfo, selectTeamInfoIsLoaded} from "../../modules/teams/teamsSelector";
import {selectPlayers, selectPlayersIsLoaded} from "../../modules/players/playersSelector";
import {useSelector} from "react-redux";

export const DetailTeam = () => {
    const dispatch = useAppDispatch();
    const id = Number(useParams<{ id: string }>().id);
    useEffect( () => {
        dispatch(fetchTeamById(id));
        dispatch(fetchPlayersByTeamId(id));
    }, [dispatch, id]);
    const team = useSelector(selectTeamInfo);
    const players = useSelector(selectPlayers);
    const playersIsLoaded = useSelector(selectPlayersIsLoaded);
    const teamIsLoaded = useSelector(selectTeamInfoIsLoaded);
    return (
        <>
            { playersIsLoaded && teamIsLoaded
                ?   <>
                        <TeamDetailCard team={team}/>
                        <Table players={players}/>
                    </>
                : <h1>Loading...</h1>
            }
        </>
    );
};
