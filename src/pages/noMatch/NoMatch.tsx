import React, { FC } from 'react';
import styled from "styled-components";
import picture404 from '../../assets/images/notFound_pic.svg';

export const NoMatch: FC = () => {
    return (
        <StyledContainer>
            <img src={picture404} alt='404'/>
            <StyledHeader>Page not found</StyledHeader>
            <StyledText>Sorry, we can’t find what you’re looking for</StyledText>

        </StyledContainer>
    );
};

const StyledContainer = styled.div`
    height: 100vh;
    width: 100vw;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  @media (${ props => props.theme.media.desktop }) {
    & img {
      height: 212px;
      width: 380px;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    & img {
      height: 153px;
      width: 275px;
    }
  }
`;
const StyledHeader = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    font-size: 36px;
    line-height: 49px;
    margin: 56px 0 24px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 17px;
    line-height: 25px;
    margin: 52px 0 16px 0;
  }
  font-weight: 800;
  color: ${props => props.theme.colors.lightestAccent};
`;
const StyledText = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    font-size: 24px;
    line-height: 33px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 15px;
    line-height: 24px;
  }
  color: ${props => props.theme.colors.primary};
`;
