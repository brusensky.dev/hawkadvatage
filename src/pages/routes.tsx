import React, {FC} from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import { PrivateRoute } from "./privateRoute/PrivateRoute";
import { Authorization } from "./authorization/Authorization";
import { App } from '../App';
import { DashboardTeams } from "./dashboardTeams/DashboardTeams";
import { AddTeam } from "./addTeam/AddTeam";
import { AddPlayer } from "./addPlayer/AddPlayer";
import { DetailTeam } from "./detailTeam/DetailTeam";
import { DetailPlayer } from "./detailPlayer/DetailPlayer";
import { NoMatch } from "./noMatch/NoMatch";
import { DashboardPlayers } from "./dashboardPlayers/DashboardPlayers";
import { EditTeam } from "./editTeam/EditTeam";
import { EditPlayer } from "./editPlayer/EditPlayer";
import { UnAuthRoute } from "./unAuthRoute/UnAuthRoute";

export interface IRoutes {
    path: string,
    component: FC,
    exact?: boolean;
    routes?: IRoutes[],
}

const appRoutes = [
    {
        path: '/teams',
        exact: true,
        component: DashboardTeams,
    },
    {
        path: '/teams/detail/:id',
        exact: true,
        component: DetailTeam,
    },
    {
        path: '/teams/edit/:id',
        exact: true,
        component: EditTeam,
    },
    {
        path: '/teams/add',
        exact: true,
        component: AddTeam,
    },
    {
        path: '/players',
        exact: true,
        component: DashboardPlayers,
    },
    {
        path: '/players/detail/:id',
        exact: true,
        component: DetailPlayer,
    },
    {
        path: '/players/edit/:id',
        exact: true,
        component: EditPlayer,
    },
    {
        path: '/players/add',
        exact: true,
        component: AddPlayer,
    },
];
export const Routes: FC = () => {
    return (
        <Switch>
            <Route exact path='/'>
                <Redirect to='/teams'/>
            </Route>
            <UnAuthRoute exact path={'/auth/sign-in'}>
                <Authorization signIn/>
            </UnAuthRoute>
            <UnAuthRoute exact path={'/auth/sign-up'}>
                <Authorization/>
            </UnAuthRoute>
            <PrivateRoute path={'/'}>
                <App routes={appRoutes}/>
            </PrivateRoute>
            <Route>
                <NoMatch/>
            </Route>
        </Switch>
    );
};
