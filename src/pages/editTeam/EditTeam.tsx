import React, {FC, useCallback, useEffect, useState} from 'react';
import { useForm } from "react-hook-form";
import { useHistory, useParams } from "react-router-dom";
import { useAppDispatch } from "../../core/redux/hooks";
import { editTeam, getDefaultValues } from "../../modules/teams/teamsThunk";
import { TeamForm } from "../../ui/teamForm/TeamForm";
import {selectTeamInfo, selectTeamInfoIsLoaded} from "../../modules/teams/teamsSelector";
import {useSelector} from "react-redux";
import {TeamFormData} from "../addTeam/AddTeam";

export const EditTeam: FC = () => {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const teamId = Number(useParams<{ id: string }>().id);
    const defaultValues = useSelector(selectTeamInfo);
    const {
        control,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm<TeamFormData>({
        defaultValues: defaultValues,
    });
    useEffect( () => {
        dispatch(getDefaultValues({
            teamId,
            reset
        }));
    }, [dispatch, teamId, reset]);
    const [imageFile, setImageFile] = useState<File>();
    const getImageFile = useCallback( (imageFile: File) => setImageFile(imageFile), []);
    const onSuccessEditing = useCallback( () => history.push(`/teams/detail/${defaultValues.id}`), [history, defaultValues]);
    const onSubmit = (data: TeamFormData) => {
        dispatch(editTeam({
            data: {
                ...data,
                id: defaultValues.id
            },
            imageFile,
            onSuccess: onSuccessEditing,
        }));
    };
    const onClickCancel = useCallback( () => history.push(`/teams/detail/${defaultValues.id}`), [history, defaultValues]);
    const detailsIsLoaded = useSelector(selectTeamInfoIsLoaded);
    return (
        <>
            { detailsIsLoaded
                ? <TeamForm
                    errors={errors}
                    onSubmit={handleSubmit(onSubmit)}
                    defaultValues={defaultValues}
                    onClickCancel={onClickCancel}
                    getImageFile={getImageFile}
                    control={control}
                />
                : <h1>Loading...</h1>
            }
        </>
    );
};
