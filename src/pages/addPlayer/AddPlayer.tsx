import React, {FC, useCallback, useEffect, useState} from 'react';
import { useForm } from "react-hook-form";
import { INewPlayerDTO } from "../../api/dto/IPlayer";
import { useAppDispatch } from "../../core/redux/hooks";
import { useHistory } from "react-router-dom";
import { addNewPlayer, fetchPositions } from "../../modules/players/playersThunk";
import { fetchTeams } from "../../modules/teams/teamsThunk";
import { PlayerForm } from "../../ui/playerForm/PlayerForm";
import {useSelector} from "react-redux";
import {selectPositions} from "../../modules/players/playersSelector";
import {selectTeamNames} from "../../modules/teams/teamsSelector";

export interface PlayerFormData extends INewPlayerDTO {}

export const AddPlayer: FC = () => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    useEffect( () => {
        dispatch(fetchPositions());
        dispatch(fetchTeams({}));
    },[dispatch]);
    const positions = useSelector(selectPositions);
    const teams = useSelector(selectTeamNames);
    const {
        handleSubmit,
        control,
        formState: { errors }
    } = useForm<PlayerFormData>();
    const onSuccessAddition = useCallback( () => history.push('/players'), [history])
    const [imageFile, setImageFile] = useState<File>();
    const getImageFile = useCallback( (imageFile: File) => setImageFile(imageFile), []);
    const onSubmit = (data: PlayerFormData) => {
        dispatch(addNewPlayer({
            data,
            imageFile,
            onSuccess: onSuccessAddition,
        }));
    };
    const onClickCancel = useCallback( () => history.push('/players'), [history] );
    return (
        <PlayerForm
            onSubmit={handleSubmit(onSubmit)}
            onClickCancel={onClickCancel}
            errors={errors}
            control={control}
            positions={positions}
            teams={teams}
            getImageFile={getImageFile}
        />
    );
};
