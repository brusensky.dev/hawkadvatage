import {ITeam} from "../api/dto/ITeam";

export const getAge = (dateString: string) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const month = today.getMonth() - birthDate.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
export const getTeamName = ( teamId: number, teams: ITeam[] ) => {
    return teams.filter( team => team.id === teamId )[0]?.name;
}
