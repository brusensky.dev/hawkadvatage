import React, {FC, useCallback, useEffect, useState} from 'react';
import { useForm } from "react-hook-form";
import { useAppDispatch } from "../../core/redux/hooks";
import { useHistory, useParams } from "react-router-dom";
import { editPlayer, fetchPositions, getDefaultValues } from "../../modules/players/playersThunk";
import { fetchTeams } from "../../modules/teams/teamsThunk";
import { PlayerForm } from "../../ui/playerForm/PlayerForm";
import {selectTeamNames, selectTeamsIsLoaded} from "../../modules/teams/teamsSelector";
import {
    selectPlayerInfo,
    selectPlayerInfoIsLoaded,
    selectPositionIsLoaded,
    selectPositions
} from "../../modules/players/playersSelector";
import {useSelector} from "react-redux";
import {PlayerFormData} from "../addPlayer/AddPlayer";

export const EditPlayer: FC = () => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const playerId = Number(useParams<{ id: string }>().id);
    const defaultValues = useSelector(selectPlayerInfo);
    const positions = useSelector(selectPositions);
    const teams = useSelector(selectTeamNames);
    const {
        handleSubmit,
        control,
        reset,
        formState: { errors }
    } = useForm<PlayerFormData>({
        defaultValues: {...defaultValues},
    });
    useEffect( () => {
        dispatch(getDefaultValues({
            playerId,
            reset}
        ));
        dispatch(fetchPositions());
        dispatch(fetchTeams({}));
    },[dispatch, playerId, reset]);
    const onSuccessEditing = useCallback( () => history.push(`/players/detail/${defaultValues.id}`), [history, defaultValues]);
    const [imageFile, setImageFile] = useState<File>();
    const getImageFile = useCallback( (imageFile: File) => setImageFile(imageFile), []);
    const onSubmit = (data: PlayerFormData) => {
        dispatch(editPlayer({
            data: {
                ...data,
                id: defaultValues.id
            },
            imageFile,
            onSuccess: onSuccessEditing
        }));
    };
    const onClickCancel = useCallback( () => history.push(`/players/detail/${defaultValues.id}`), [history, defaultValues]);
    const positionsIsLoaded = useSelector( selectPositionIsLoaded );
    const teamsIsLoaded = useSelector( selectTeamsIsLoaded );
    const detailsIsLoaded = useSelector( selectPlayerInfoIsLoaded );
    return (
        <>
            { positionsIsLoaded && teamsIsLoaded && detailsIsLoaded
                ? <PlayerForm
                    onSubmit={handleSubmit(onSubmit)}
                    onClickCancel={onClickCancel}
                    defaultValues={defaultValues}
                    errors={errors}
                    control={control}
                    positions={positions}
                    teams={teams}
                    getImageFile={getImageFile}
                />
                : <h1>Loading...</h1>
            }
        </>
    );
};
