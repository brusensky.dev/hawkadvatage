import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStyle } from './GlobalStyles';
import { ThemeProvider } from "styled-components";
import { Provider } from 'react-redux';
import { store } from './core/redux/store';
import { theme } from "./assets/theme/theme";
import { BrowserRouter } from "react-router-dom";
import { Routes } from "./pages/routes";


ReactDOM.render(
    <Provider store={store}>
      <ThemeProvider theme={theme}>
          <GlobalStyle/>
          <BrowserRouter>
              <Routes/>
          </BrowserRouter>
      </ThemeProvider>
    </Provider>,
  document.getElementById('root')
);
