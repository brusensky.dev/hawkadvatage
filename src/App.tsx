import React, {FC, useState} from 'react';
import { Header } from "./ui/header/Header";
import { Content } from "./ui/content/Content";
import { SidebarMenu } from './ui/sidebarMenu/SidebarMenu';
import styled from "styled-components";
import {Switch} from "react-router-dom";
import {IRoutes} from "./pages/routes";
import {PrivateRoute} from "./pages/privateRoute/PrivateRoute";

interface IAppProps {
    routes: IRoutes[],
}

export const App: FC<IAppProps> = ( {routes} ) => {
    const [ isSidebarShown, setSidebarShown ] = useState(false);
    return (
      <StyledLayout>
          <Header showSidebar={setSidebarShown}/>
          <SidebarMenu
              isSidebarShown={isSidebarShown}
              showSidebar={setSidebarShown}
          />
          <Content>
              <Switch>
                  { routes.map( (route, i) => (
                      <PrivateRoute
                          key={i}
                          path={route.path}
                          exact={route.exact}
                      >
                          <route.component/>
                      </PrivateRoute>
                  ))}
              </Switch>
          </Content>
      </StyledLayout>
    );
}
const StyledLayout = styled.div`
  @media (${ props => props.theme.media.XXL }) {
    display: grid;
    height: 100vh;
    grid-template-areas: "header header"
      "navigation main";
    grid-template-rows: 80px 1fr;
    grid-template-columns: 140px 1fr;
  }
  @media (${ props => props.theme.media.xLarge }) {
    display: grid;
    height: 100vh;
    grid-template-areas: "header"
      "main";
    grid-template-rows: 80px 1fr;
  }
  @media (${ props => props.theme.media.phone }) {
    grid-template-rows: 58px 1fr;
  }
`;
