import { createAsyncThunk } from '@reduxjs/toolkit';
import {addPlayer, deletePlayer, getPlayer, getPlayers, getPositions, updatePlayer} from '../../api/requests/player';
import { saveImage } from "../../api/requests/image";
import { INewPlayerDTO, IPlayer } from "../../api/dto/IPlayer";

type TGetPlayers = {
    name?: string,
    page?: number,
    pageSize?: number,
    teamIds?: number[],
}

export const fetchPositions = createAsyncThunk(
    'players/getPositions',
    async () => {
        return await getPositions();
    }
);
export const fetchPlayersByTeamId = createAsyncThunk(
    'players/getPlayersByTeamId',
    async (teamId: number) => {
        return await getPlayers(undefined , [teamId]);
    }
);
export const fetchPlayers = createAsyncThunk(
    'players/getPlayers',
    async ({name, page, pageSize, teamIds}: TGetPlayers) => {
        return await getPlayers(name, teamIds, page, pageSize);
    }
);
export const fetchPlayerById = createAsyncThunk(
    'players/getPlayerById',
    async (playerId: number) => {
        return await getPlayer(playerId);
    }
);
export const getDefaultValues = createAsyncThunk(
    'players/getPlayerById',
    async ({playerId, reset}: { playerId: number, reset: any}) => {
        const response = await getPlayer(playerId);
        reset(response);
        return response;
    }
);
export const addNewPlayer = createAsyncThunk(
    'players/addPlayer',
    async ({data, imageFile, onSuccess}: { data: INewPlayerDTO, imageFile: any, onSuccess: () => void}) => {
        if (imageFile)
            await saveImage(imageFile).then( result =>  data.avatarUrl = result);
        else
            data.avatarUrl = null;
        await addPlayer(data).then(onSuccess);
    },
);
export const editPlayer = createAsyncThunk(
    'players/updatePlayer',
    async ({data, imageFile, onSuccess}: { data: IPlayer, imageFile: any, onSuccess: () => void}) => {
        if (imageFile)
            await saveImage(imageFile).then( result =>  data.avatarUrl = result);
        await updatePlayer(data).then(onSuccess);
    },
);
export const deletePlayerById = createAsyncThunk(
    'players/deletePlayers',
    async ({id, onSuccess}:{id: number, onSuccess: () => void}) => {
        await deletePlayer(id).then(onSuccess);
    }
);
