import {createSlice} from "@reduxjs/toolkit";
import { IPlayer } from "../../api/dto/IPlayer";
import { fetchPlayerById, fetchPlayers, fetchPlayersByTeamId, fetchPositions } from './playersThunk';

const initialState = {
    playersList: [] as IPlayer[],
    count: 0,
    page: 0,
    pageSize: 0,
    positions: [] as string[],
    detailPlayerData: {} as IPlayer,
    positionsIsLoaded: false,
    detailPlayerDataIsLoaded: false,
    isLoaded: false,
    hasError: false,
}
export const playersSlice = createSlice({
    name: 'players',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase( fetchPositions.pending, (state) => {
            state.positionsIsLoaded = false;
            state.hasError = false;
        });
        builder.addCase( fetchPositions.fulfilled, (state, action) => {
            state.positionsIsLoaded = true;
            state.positions = action.payload;
        });
        builder.addCase( fetchPlayersByTeamId.fulfilled, (state, action) => {
            state.isLoaded = true;
            state.playersList = action.payload.data;
            state.count = action.payload.count;
            state.page = action.payload.page;
            state.pageSize = action.payload.size;
        });
        builder.addCase( fetchPlayers.pending, (state) => {
            state.isLoaded = false;
            state.hasError = false;
        });
        builder.addCase( fetchPlayers.fulfilled, (state, action) => {
            state.isLoaded = true;
            state.playersList = action.payload.data;
            state.count = action.payload.count;
            state.page = action.payload.page;
            state.pageSize = action.payload.size;
        });
        builder.addCase( fetchPlayers.rejected, (state) => {
            state.isLoaded = true;
            state.hasError = true;
        });
        builder.addCase( fetchPlayerById.pending, (state) => {
            state.detailPlayerDataIsLoaded = false;
            state.hasError = false;
        });
        builder.addCase( fetchPlayerById.fulfilled, (state, action) => {
            state.detailPlayerDataIsLoaded = true;
            state.detailPlayerData = action.payload;
        });
        builder.addCase( fetchPlayerById.rejected, (state) => {
            state.detailPlayerDataIsLoaded = true;
            state.hasError = true;
        });

    },
});
export const playersReducer = playersSlice.reducer;
