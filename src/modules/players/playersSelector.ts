import {createSelector} from "@reduxjs/toolkit";
import {RootState} from "../../core/redux/rootReducer";

export const selectPositions = createSelector(
    (state: RootState) => state.players.positions,
    (positions) => {
        return positions.map( position => ({
            label: position,
            value: position,
        }));
    }
);
export const selectPlayers = (state: RootState) => state.players.playersList;
export const selectPlayersIsLoaded = (state: RootState) => state.players.isLoaded;
export const selectPositionIsLoaded = (state: RootState) => state.players.positionsIsLoaded;
export const selectPlayerInfo = (state: RootState) => state.players.detailPlayerData;
export const selectPlayerInfoIsLoaded = (state: RootState) => state.players.detailPlayerData;
export const selectNumberOfPlayers = (state: RootState) => state.players.count;



