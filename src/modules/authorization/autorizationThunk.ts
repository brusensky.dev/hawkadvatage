import {createAsyncThunk} from "@reduxjs/toolkit";
import {login, signUp} from "../../api/requests/authorization";
import {IAuthorization, IRegistration} from "../../api/dto/IAuthorization";

export const authSignIn = createAsyncThunk(
    'authorization/signIn',
    async (data: IAuthorization) => {
        return await login(data);
    }
);
export const authSignUp = createAsyncThunk(
    'authorization/signUp',
    async (data: IRegistration) => {
        return await signUp(data);
    }
);
