import {RootState} from "../../core/redux/rootReducer";

export const selectErrorStatus = (state: RootState) => state.auth.authError.status;
export const selectCurrentUser = (state: RootState) => state.auth.currentUser;
