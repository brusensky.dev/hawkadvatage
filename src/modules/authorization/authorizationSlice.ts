import { createSlice } from "@reduxjs/toolkit";
import { authSignIn, authSignUp } from "./autorizationThunk";

const initialState = {
    isLoggedIn: false,
    currentUser: {
        name: '',
        avatarUrl: null,
        authToken: '',
    },
    authError: {
        type: '',
        title: '',
        status:	0,
        traceId: '',
    },
}
export const authorizationSlice = createSlice({
    name: 'authorization',
    initialState,
    reducers: {
        setLoggedOut: (state) => {
            state.isLoggedIn = false;
        },
        clearUser: (state) => {
            state.currentUser = {
                name: '',
                avatarUrl: null,
                authToken: '',
            };
        },
        clearAuthError: (state) => {
            state.authError = {
                type: '',
                title: '',
                status:	0,
                traceId: '',
            };
        },
    },
    extraReducers: (builder) => {
        builder.addCase( authSignIn.pending, (state) => {
            state.isLoggedIn = false;
        });
        builder.addCase( authSignIn.fulfilled, (state, action) => {
            if(action.payload.status) {
                state.authError = action.payload;
            } else {
                state.authError = {type:'',title:'',status:0,traceId:''};
                state.currentUser = action.payload;
                localStorage.setItem('token', action.payload.token);
                state.isLoggedIn = true;
            }
        });
        builder.addCase( authSignIn.rejected, (state, action) => {
            state.isLoggedIn = false;
            console.log(action.payload, 'rejected');
        });
        builder.addCase( authSignUp.pending, (state) => {
            state.isLoggedIn = false;
        });
        builder.addCase( authSignUp.fulfilled, ( state, action ) => {
            state.currentUser = action.payload;
            localStorage.setItem('token', state.currentUser.authToken);
            state.isLoggedIn = true;
        });
    }
});
export const { setLoggedOut, clearUser } = authorizationSlice.actions;
export const authReducer = authorizationSlice.reducer;
