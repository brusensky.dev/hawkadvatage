import { createAsyncThunk } from "@reduxjs/toolkit";
import {addTeam, deleteTeam, getTeam, getTeams, updateTeam} from "../../api/requests/team";
import { INewTeamDTO, ITeam } from "../../api/dto/ITeam";
import { saveImage } from "../../api/requests/image";

type TGetTeams = {
    name?: string,
    page?: number,
    pageSize?: number,
}

export const fetchTeams = createAsyncThunk(
    'teams/getTeams',
    async ( { name, page, pageSize }: TGetTeams) => {
        return await getTeams(name, page, pageSize);
    },
);
export const fetchTeamById = createAsyncThunk(
    'teams/getTeamById',
    async (teamId: number) => {
        return await getTeam(teamId);
    },
);
export const getDefaultValues = createAsyncThunk(
    'teams/getTeamById',
    async ({teamId, reset}: { teamId: number, reset: any}) => {
        const response = await getTeam(teamId);
        reset(response);
        return response;
    }
);
export const addNewTeam = createAsyncThunk(
    'teams/addTeam',
    async ({data, imageFile, onSuccess}: { data: INewTeamDTO, imageFile: any, onSuccess: () => void}) => {
        if (imageFile)
            await saveImage(imageFile).then( result =>  data.imageUrl = result);
        else
            data.imageUrl = null;
        await addTeam(data).then(onSuccess);
    },
);
export const editTeam = createAsyncThunk(
    'teams/updateTeam',
    async ({data, imageFile, onSuccess}: { data: ITeam, imageFile: any, onSuccess: () => void}) => {
        if (imageFile)
            await saveImage(imageFile).then( result =>  data.imageUrl = result);
        await updateTeam(data).then(onSuccess);
    },
);
export const deleteTeamById = createAsyncThunk(
    'teams/deleteTeam',
    async ({id, onSuccess}:{id: number, onSuccess: () => void}) => {
        await deleteTeam(id).then(onSuccess);
    }
);
