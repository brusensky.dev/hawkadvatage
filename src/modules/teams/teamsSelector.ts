import {createSelector} from "@reduxjs/toolkit";
import {RootState} from "../../core/redux/rootReducer";

export const selectTeamNames = createSelector(
    (state: RootState) => state.teams.teamsList,
    (teams) => {
        return teams.map( team => ({
            label: team.name,
            value: team.id.toString()
        }));
    }
);
export const selectTeams = (state: RootState) => state.teams.teamsList;
export const selectTeamsIsLoaded = (state: RootState) => state.teams.isLoaded;
export const selectTeamInfo = (state: RootState) => state.teams.detailTeamData;
export const selectTeamInfoIsLoaded = (state: RootState) => state.teams.detailTeamDataIsLoaded;
export const selectNumberOfTeams = (state: RootState) => state.teams.count;
