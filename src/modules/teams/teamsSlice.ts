import { createSlice } from "@reduxjs/toolkit";
import {ITeam} from "../../api/dto/ITeam";
import {fetchTeamById, fetchTeams} from "./teamsThunk";

const initialState = {
    teamsList: [] as ITeam[],
    count: 0,
    page: 0,
    pageSize: 0,
    detailTeamData: {} as ITeam,
    detailTeamDataIsLoaded: false,
    isLoaded: false,
    hasError: false,
}
export const teamsSlice = createSlice({
    name: 'teams',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase( fetchTeams.pending, (state) => {
            state.isLoaded = false;
        });
        builder.addCase( fetchTeams.fulfilled, (state, action) => {
            state.isLoaded = true;
            state.teamsList = action.payload.data;
            state.count = action.payload.count;
            state.page = action.payload.page;
            state.pageSize = action.payload.size;
        });
        builder.addCase( fetchTeams.rejected, (state) => {
            state.isLoaded = true;
            state.hasError = true;
        });
        builder.addCase( fetchTeamById.pending, (state) => {
            state.detailTeamDataIsLoaded = false;
            state.hasError = false;
        });
        builder.addCase( fetchTeamById.fulfilled, (state, action) => {
            state.detailTeamDataIsLoaded = true;
            state.detailTeamData = action.payload;
        });
        builder.addCase( fetchTeamById.rejected, (state) => {
            state.detailTeamDataIsLoaded = true;
            state.hasError = true;
        });
    },
});
export const teamsReducer = teamsSlice.reducer;
