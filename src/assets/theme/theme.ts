export const theme = {
    colors: {
        darkPrimary: '#303030',
        primary: '#707070',
        lightPrimary: '#9c9c9c',
        lightPrimaryRgb: '156, 156, 156',
        lightestPrimary: '#d1d1d1',
        lightestPrimary1: '#f6f6f6',
        darkAccent: '#c60e2e',
        accent: '#e4163a',
        lightAccent: '#ff5761',
        lightestAccent: '#ff768e',
        basic: '#ffffff',
        blue: '#344472',
    },
    media: {
        phone: 'max-width: 575px',
        desktop: 'min-width: 576px',
        medium: 'max-width: 815px',
        large: 'max-width: 1200px',
        xLarge: 'max-width: 1340px',
        XXL: 'min-width: 1341px',
    }
};
