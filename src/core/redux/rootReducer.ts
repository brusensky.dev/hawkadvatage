import { authReducer } from '../../modules/authorization/authorizationSlice'
import { combineReducers } from "@reduxjs/toolkit";
import { teamsReducer } from "../../modules/teams/teamsSlice";
import { playersReducer } from "../../modules/players/playersSlice";

export const rootReducer = combineReducers({
    auth: authReducer,
    teams: teamsReducer,
    players: playersReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
