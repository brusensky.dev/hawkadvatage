import React, {FC, useCallback} from 'react';
import styled, {css} from 'styled-components';
import playersIcon from '../../assets/icon/person_rounded.svg';
import playersSelectedIcon from '../../assets/icon/person_selected.svg';
import teamsIcon from '../../assets/icon/group_person_rounded.svg';
import teamsSelectedIcon from '../../assets/icon/group_person_selected.svg';
import signOutIcon from '../../assets/icon/sign_out.svg';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { MenuItem } from "./MenuItem";
import { clearUser, setLoggedOut } from "../../modules/authorization/authorizationSlice";
import { useAppDispatch } from "../../core/redux/hooks";
import { User } from "../user/User";

interface IProps {
    isSidebarShown: boolean,
    showSidebar: (arg0: boolean) => any;
}

export const SidebarMenu: FC<IProps> = ({isSidebarShown, showSidebar}) => {
    const onClick = useCallback(() => showSidebar(false), [showSidebar]);
    const history = useHistory();
    const location = useLocation();
    const dispatch = useAppDispatch();
    const logOut = useCallback(() => {
        dispatch(setLoggedOut());
        dispatch(clearUser());
        localStorage.removeItem('token');
        history.push('/');
    }, [dispatch, history]);
    return (
        <NavContainer
            isSidebarShown={isSidebarShown}
            showSidebar={showSidebar}
        >
            <Overlay className='nav__overlay' onClick={onClick}/>
            <NavWrapper className='nav__links'>
                <UserToggle>
                    <User/>
                </UserToggle>
                <Nav>
                    <MenuList>
                        <CustomLink to={'/teams'}>
                            <MenuItem selectedItem={location.pathname.includes('teams')} >
                                <img
                                    src={ location.pathname.includes('teams')
                                        ? teamsSelectedIcon
                                        : teamsIcon
                                    }
                                     alt="#"
                                />
                                <div>Teams</div>
                            </MenuItem>
                        </CustomLink>
                        <CustomLink to={'/players'}>
                            <MenuItem selectedItem={location.pathname.includes('players')} >
                                <img
                                    src={ location.pathname.includes('players')
                                        ? playersSelectedIcon
                                        : playersIcon }
                                    alt="#"
                                />
                                <div>Players</div>
                            </MenuItem>
                        </CustomLink>
                    </MenuList>
                    <SignOutButton onClick={logOut}>
                        <img src={signOutIcon} alt="X"/>
                        <div>Sign out</div>
                    </SignOutButton>
                </Nav>
            </NavWrapper>
        </NavContainer>
    );
};
const NavContainer = styled.div`
  @media (${ props => props.theme.media.XXL }) {
    grid-area: navigation;
  }
  @media (${ props => props.theme.media.xLarge }) {
    display: none;
    position: fixed;
    z-index: 5;
    top: 0;
    left: 0;
    padding-top: 80px;
    width: 201px;
    height: 100vh;
  };
  ${ ( props: IProps ) => props.isSidebarShown && css`
    @media (${ props => props.theme.media.xLarge }) {
      display: block;
      & .nav__links {
        transform: translateX(0);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
      }
      & .nav__overlay {
        visibility: visible;
        opacity: 1;
      }
    }
  `};
  @media (${ props => props.theme.media.phone }) {
    padding-top: 58px;
  }
`;
const NavWrapper = styled.div`
  height: 100%;
  @media (${ props => props.theme.media.xLarge }) {
    position: relative;
    z-index: 5;
    background: ${props => props.theme.colors.basic};
    transform: translateX(-201px);
    transition: transform 0.3s;
  }
`;
const Overlay = styled.div`
  @media (${ props => props.theme.media.XXL }) {
    display: none;
  }
  @media (${ props => props.theme.media.xLarge }) {
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100vw;
    background: rgba(65, 65, 65, 0.6);
    z-index: 2;
    visibility: hidden;
    opacity: 0;
    transition: opacity 0.3s;
  }
`;
const Nav = styled.nav`
  @media (${ props => props.theme.media.desktop }) {
    color: ${props => props.theme.colors.lightPrimary};
    padding: 32px 0 32px 0;
    height: 100%;
    width: 140px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
  }
  @media (${ props => props.theme.media.xLarge }) {
    position: relative;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    padding: 24px;
  }
`;
const MenuList = styled.ul`
  font-size: 12px;
  line-height: 18px;
`;

const SignOutButton = styled.button`
  font-size: 12px;
  line-height: 18px;
  bottom: 24px;
  color: ${props => props.theme.colors.lightestAccent};
  background: transparent;
  img {
    width: 24px;
    height: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    display: flex;
    position: fixed;
    bottom: 24px;
    align-items: center;
    img {
        margin-right: 8px;
    }
`;
const CustomLink = styled(Link)`
    & div {
      text-decoration: none;
    }
    & div:focus, & div:hover, & div:visited, & div:link, & div:active {
      text-decoration: none;
    }
`;
const UserToggle = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    display: none;
  }
  @media (${ props => props.theme.media.medium }) {
    display: flex;
    border-bottom: 1px solid ${props => props.theme.colors.lightestPrimary};
    height: 80px;
    width: 100%;
    align-items: center;
  }
`;
