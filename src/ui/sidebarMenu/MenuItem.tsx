import React, {FC} from 'react';
import styled, {css} from "styled-components";

interface IProps {
    selectedItem: boolean,
}

export const MenuItem: FC<IProps> = (props) => {
    return (
        <StyledMenuItem {...props}>
            {props.children}
        </StyledMenuItem>
    );
}

const StyledMenuItem =  styled.li`
  display: flex;
  align-items: center;
  margin-bottom: 36px;
  cursor: pointer;
  img {
    width: 24px;
    height: 24px;
    @media (${ props => props.theme.media.phone }) {
      margin-right: 8px;
    }
  }
  div {
    color: ${ props => props.theme.colors.lightPrimary };
    ${ (props: IProps) => props.selectedItem && css`
      color: ${ props => props.theme.colors.accent };
    `};
  }
  @media (${ props => props.theme.media.desktop }) {
    flex-direction: column;
  }
  @media (${ props => props.theme.media.phone }) {
    flex-direction: row;
  }
`;
