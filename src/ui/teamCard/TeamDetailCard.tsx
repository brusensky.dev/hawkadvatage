import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import deleteIcon from '../../assets/icon/delete_rounded.svg';
import editIcon from '../../assets/icon/create.svg';
import { ITeam } from "../../api/dto/ITeam";
import { Link, useHistory } from "react-router-dom";
import {deleteTeamById} from "../../modules/teams/teamsThunk";
import {useAppDispatch} from "../../core/redux/hooks";

interface IProps {
    team: ITeam
}

export const TeamDetailCard: FC<IProps> = ({team}) => {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const onSuccessDeletion = useCallback( () => history.push('/teams'), [history]);
    const onClickDelete = useCallback(() => {
        dispatch(deleteTeamById({
            id: team.id,
            onSuccess: onSuccessDeletion,
        }));
    }, [dispatch, team.id, onSuccessDeletion]);
    const onClickEdit = useCallback(() => history.push(`/teams/edit/${team.id}`), [history, team.id]);
    return (
        <StyledCard>
            <CardHeader>
                <PathBlock>
                    <Link to={'/teams'}>
                        <PathText>Teams</PathText>
                    </Link>
                    &nbsp;/&nbsp;
                    <PathText>{team.name}</PathText>
                </PathBlock>
                <ActionsBlock>
                    <img
                        src={editIcon}
                        alt="create"
                        onClick={onClickEdit}
                    />
                    <img
                        src={deleteIcon}
                        alt="delete"
                        onClick={ onClickDelete }
                    />
                </ActionsBlock>
            </CardHeader>
            <CardContent>
                <TeamLogo>
                    <img
                        src={team.imageUrl || undefined}
                        alt={team.name}
                    />
                </TeamLogo>
                <TeamInfoContainer>
                    <TeamInfoName>{team.name}</TeamInfoName>
                    <TeamInfoList>
                        <TeamInfoItem>
                            <div className="title">Year of foundation</div>
                            <div className="value">{team.foundationYear}</div>
                        </TeamInfoItem>
                        <TeamInfoItem>
                            <div className="title">Division</div>
                            <div className="value">{team.division}</div>
                        </TeamInfoItem>
                        <TeamInfoItem>
                            <div className="title">Conference</div>
                            <div className="value">{team.conference}</div>
                        </TeamInfoItem>
                    </TeamInfoList>
                </TeamInfoContainer>
            </CardContent>
        </StyledCard>
    );
};
const StyledCard = styled.div`
  overflow: hidden;
  @media (${ props => props.theme.media.desktop }) {
    width: 1140px;
    border-radius: 10px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 752px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
    width: 100vw;
  }
`;
const CardHeader = styled.div`
    border: 0.5px solid ${ props => props.theme.colors.lightPrimary };
    display: flex;
    justify-content: space-between;
  @media (${ props => props.theme.media.desktop }) {
    height: 70px;
    border-radius: 10px 10px 0 0;
    padding: 24px 32px 0 32px;
    line-height: 24px;
    font-size: 14px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 48px;
    padding: 12px 16px;
    line-height: 18px;
    font-size: 13px;
  }
`;
const PathBlock = styled.div``;
const PathText = styled.span`
    color: ${ props => props.theme.colors.accent };
`;
const ActionsBlock = styled.div`
    width: 64px;
    display: flex;
    justify-content: space-between;
    & img {
      height: 24px;
      width: 24px;
      cursor: pointer;
    }
`;
const CardContent = styled.div`
    color: ${ props => props.theme.colors.basic };
    background: linear-gradient(276.45deg, #707070 0%, #393939 100.28%);
    display: flex;
  @media (${ props => props.theme.media.desktop }) {
    height: 404px;
    padding: 66px 0 0 0;
  }
  @media (${ props => props.theme.media.large }) {
    height: auto;
    flex-direction: column;
    align-items: center;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 48px 0;
    flex-direction: column;
    align-items: center;
  }
`;
const TeamLogo = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    height: 210px;
    width: 210px;
    margin: auto 146px 97px 146px;
    & img {
      height: 210px;
      width: 210px;
    }
  }
  @media (${ props => props.theme.media.large }) {
    margin: auto 76px 40px 76px;
  }
  @media (${ props => props.theme.media.phone }) {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 90px;
    width: 89px;
    margin-bottom: 48px;
    & img {
      height: 90px;
      width: 89px;
    }
  }
    
`;
const TeamInfoContainer = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    width: 600px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 80%;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 100vw;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
const TeamInfoName = styled.div`
  font-weight: 800;
  @media (${ props => props.theme.media.desktop }) {
    font-size: 36px;
    line-height: 49px;
  }
  @media (${ props => props.theme.media.large }) {
    text-align: center;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 17px;
    line-height: 25px;
  }
`;
const TeamInfoList = styled.div`
  display: flex;
  padding: 40px 0 0 0;
  @media (${ props => props.theme.media.desktop }) {
    flex-wrap: wrap;
  }
  @media (${ props => props.theme.media.medium }) {
    flex-direction: column;
  }
`;
const TeamInfoItem = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    flex: 50%;
    margin-bottom: 54px;
    & .title {
      font-weight: 800;
      font-size: 24px;
      line-height: 33px;
      margin-bottom: 8px;
    }
    & .value {
      font-weight: 500;
      font-size: 18px;
      line-height: 25px;
    }
  }
  @media (${ props => props.theme.media.large }) {
    & .title {
      text-align: center;
    }
    & .value {
      text-align: center;
    }
  }
  @media (${ props => props.theme.media.medium }) {
    display: flex;
    flex: 0;
    flex-direction: column;
    align-items: center;
    margin-bottom: 32px;
  }
  @media (${ props => props.theme.media.phone }) {
    display: flex;
    flex-direction: column;
    align-items: center;
    & .title {
      font-weight: 800;
      font-size: 17px;
      line-height: 25px;
      margin-bottom: 8px;
    }
    & .value {
      font-weight: 500;
      font-size: 15px;
      line-height: 25px;
    }
  }
`;
