import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import { useHistory } from "react-router-dom";

interface ITeamCardProps {
    name: string,
    foundationYear: number,
    imageUrl: string,
    id: number,
}

export const TeamCard: FC<ITeamCardProps> = (props) => {
    const history = useHistory();
    const onClick = useCallback( () => history.push(`teams/detail/${props.id}`),[history, props.id]);
    return (
        <StyledCard onClick={onClick}>
            <CardImageContainer>
                <TeamLogo>
                    <img src={props.imageUrl} alt={props.name}/>
                </TeamLogo>
            </CardImageContainer>
            <CardTeamNameContainer>
                <CardTeamName>{props.name}</CardTeamName>
                <CardTeamYear>{`Year of foundation: ${props.foundationYear}`}</CardTeamYear>
            </CardTeamNameContainer>
        </StyledCard>
    );
};
const StyledCard = styled.div`
  border-radius: 4px;
  overflow: hidden;
  &:hover {
    cursor: pointer;
  }
  @media (${ props => props.theme.media.desktop }) {
    height: 380px;
    width: 364px;
    margin: 0 24px 24px 0;
    &:nth-child(3n) {
      margin: 0 0 24px 0;
    }
  }
  @media (${ props => props.theme.media.large }) {
    margin: 0 0 24px 0;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 48%;
    height: 30%;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 201px;
    width: 48%;
    margin-bottom: 12px;
  }
`;
const CardImageContainer = styled.div`
  background: linear-gradient(121.57deg, #707070 1.62%, #393939 81.02%);
  @media (${ props => props.theme.media.desktop }) {
    padding: 65px 0;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media (${ props => props.theme.media.medium }) {
    padding: 35px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 107px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
const TeamLogo = styled.div`
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (${ props => props.theme.media.desktop }) {
    height: 150px;
    width: 150px;
    & img {
      height: 150px;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    height: 51px;
    width: 58px;
    & img {
      height: 51px;
    }
  }
`;
const CardTeamNameContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.colors.darkPrimary};
  @media (${ props => props.theme.media.desktop }) {
    height: 100px;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 94px;
  }
`;
const CardTeamName = styled.div`
  color: ${props => props.theme.colors.basic};
  @media (${ props => props.theme.media.desktop }) {
    font-size: 18px;
    line-height: 25px;
    margin-bottom: 12px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 6px;
  }
`;
const CardTeamYear = styled.div`
  color: ${props => props.theme.colors.lightPrimary};
  @media (${ props => props.theme.media.desktop }) {
    font-size: 14px;
    line-height: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 13px;
    line-height: 18px;
  }
`;
