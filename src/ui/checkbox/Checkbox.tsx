import React, {FC} from 'react';
import styled, {css} from "styled-components";

interface IProps {
    label: string;
    id: string,
    errorMessage?: string | undefined,
    onChange: (...event: any[]) => void,
    disabled?: boolean;
    checked?: boolean;
}

export const Checkbox: FC<IProps> = (props) => {
    const {
        label,
        id,
        errorMessage,
        onChange,
    } = props;
    return (
        <>
            <Container
                className="container"
                htmlFor={id}
            >
                <HiddenDefaultInput
                    type="checkbox"
                    id={id}
                    onChange={onChange}
                />
                <LabelText
                    {...props}
                    className="labelText"
                >
                    {label}
                </LabelText>
                <CheckMark
                    {...props}
                    className="checkmark"
                />

            </Container>
            { errorMessage && <StyledError>{errorMessage}</StyledError> }
        </>
    );
};

const Container = styled.label`
  display: block;
  position: relative;
  padding-left: 22px;
  cursor: pointer;
  user-select: none;
  margin: 24px 0 0 0;
  
  &:hover input ~ .checkmark {
    border: 1px solid ${ props => props.theme.colors.accent };
  }
  
  /* When the checkbox is checked*/
  & > input:checked ~ .checkmark {
    background-color: ${ props => props.theme.colors.accent };
    border: 0;
  }
  /* When the checkbox is checked and disabled */
  & > input:checked:disabled ~ .checkmark {
    background-color: ${ props => props.theme.colors.lightestPrimary };
    border: 0;
  }
  & > input:checked:disabled + .lText {
    color: ${ props => props.theme.colors.lightestPrimary };
  };

  /* Show the checkmark when checked */
  & > input:checked ~ .checkmark:after {
    display: block;
  }
  /* When the checkbox is disabled */
  & > input:disabled ~ .checkmark {
    background-color: ${ props => props.theme.colors.lightestPrimary1 };
    border: 1px solid ${ props => props.theme.colors.lightestPrimary };
    color: ${ props => props.theme.colors.lightestPrimary };
  }
  & > input:disabled + .labelText {
    color: ${ props => props.theme.colors.lightestPrimary }
  }
`;
const HiddenDefaultInput = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;
const CheckMark = styled.span`
  position: absolute;
  top: 4px;
  left: 0;
  height: 12px;
  width: 12px;
  border: 1px solid ${ props => props.theme.colors.lightPrimary};
  border-radius: 2px;

  ${ (props: IProps) => props.errorMessage && css`
    border-color: ${ props => props.theme.colors.lightestAccent }
  `};

  /* Create the checkmark/indicator (hidden when not checked) */
  &:after {
    content: '';
    position: absolute;
    display: none;
  }
  /* Style the checkmark/indicator */
  &:after {
    left: 4px;
    top: 1px;
    width: 4px;);
    height: 9px;
    border: solid white;
    border-width: 0 2px 2px 0;
    transform: rotate(40deg);
  }  
`;
const LabelText = styled.span`
  font-size: 14px;
  line-height: 24px;
  color: ${ props => props.theme.colors.primary };
  
  ${ (props: IProps) => props.errorMessage && css`
    color: ${ props => props.theme.colors.lightestAccent };
  `};
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
