import React, {FC} from 'react';
import styled from "styled-components";
import { IPlayer } from "../../api/dto/IPlayer";
import { getAge } from "../../pages/utils";

interface IProps {
    players: IPlayer[],
}

export const Table: FC<IProps> = ({players}) => {
    return (
        <StyledTable>
            <TableCaption>Roster</TableCaption>
            <TableHead>
                <StyledTableRowTitle>
                    <TableCell as='th'>#</TableCell>
                    <TableCell as='th'>Player</TableCell>
                    <TableCell as='th'>Height</TableCell>
                    <TableCell as='th'>Weight</TableCell>
                    <TableCell as='th'>Age</TableCell>
                </StyledTableRowTitle>
            </TableHead>
            <TableBody>
                {players.map( player => (
                    <TableRow
                        key={player.id}
                        player={player}/>
                ))}
            </TableBody>
        </StyledTable>
    );
};
const TableRow: FC<{player: IPlayer}> = (props) => {
    return (
        <StyledTableRow>
            <TableCell className='table_cell'>{props.player.number}</TableCell>
            <TableCell className='table_cell'>
                <Player>
                    <div className='player__photo'>
                        <div className='ellipse'>
                            <img
                                src={props.player.avatarUrl || undefined}
                                alt={props.player.name}
                            />
                        </div>
                    </div>
                    <div>
                        <div className='player__name'>{props.player.name}</div>
                        <div className='player__position'>{props.player.position}</div>
                    </div>
                </Player>
            </TableCell>
            <TableCell className='table_cell'>{`${props.player.height} cm`}</TableCell>
            <TableCell className='table_cell'>{`${props.player.weight} kg`}</TableCell>
            <TableCell className='table_cell'>{getAge(props.player.birthday)}</TableCell>
        </StyledTableRow>
    );
};
const Player = styled.div`
  display: flex;
  align-items: center;
  & .ellipse {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;
  }
  & .player__name {
    font-size: 14px;
    line-height: 24px;
  }
  & .player__position {
    font-size: 12px;
    line-height: 18px;
  }
  & .player__photo {
    height: 38px;
    width: 52px;
    margin: 0 10px 0 0;
  }
  & .player__photo img {
    height: 40px;
    margin-left: -5px;
  }
`;
const StyledTable = styled.table`
  margin-top: 24px;
  color: ${ props => props.theme.colors.primary};
  border-spacing: 0;
  font-weight: 400;
  @media (${ props => props.theme.media.desktop }) {
    width: 100%;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 100vw;
  }
`;
const TableCaption = styled.caption`
  @media (${ props => props.theme.media.desktop }) {
    height: 55px;
    border-radius: 10px 10px 0 0;
    padding: 14px 32px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 48px;
    border-right: 0;
    border-left: 0;
    padding: 12px 16px;
  }
  border: 0.5px solid ${ props => props.theme.colors.lightPrimary};
  text-align: left;
  font-size: 18px;
  line-height: 25px;
`;
const TableBody = styled.tbody``;
const TableHead = styled.thead``;
const StyledTableRowTitle = styled.tr`
  @media (${ props => props.theme.media.desktop }) {
    height: 40px;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 36px;
  }
`;
const StyledTableRow = styled.tr`
  height: 55px;
  @media (${ props => props.theme.media.desktop }) {
    &:last-child .table_cell:first-child {
      border-radius: 0 0 0 10px;
    }
    &:last-child .table_cell:last-child {
      border-radius: 0 0 10px 0;
    }
  }
  @media (${ props => props.theme.media.medium }) {
    &:last-child .table_cell:first-child {
      border-radius: 0;
    }
    &:last-child .table_cell:last-child {
      border-radius: 0;
    }
  }
`;
const TableCell = styled.td`
  border-bottom: 0.5px solid ${ props => props.theme.colors.lightPrimary};
  vertical-align: center;
  text-align: left;
  font-size: 14px;
  line-height: 24px;
  &:nth-child(2) {
    text-align: left;
    width: 67%;
    padding: 0 0 0 32px;
  }
  @media (${ props => props.theme.media.desktop }) {
    &:first-child {
      border-left: 0.5px solid ${ props => props.theme.colors.lightPrimary};
      padding: 0 0 0 32px;
      width: 40px;
    }
    &:last-child {
      border-right: 0.5px solid ${ props => props.theme.colors.lightPrimary};
    }
  }
  @media (${ props => props.theme.media.medium }) {
    &:nth-child(2) {
      width: auto;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    &:first-child {
      padding: 0 0 0 32px;
      width: 40px;
    }
    &:nth-child(3), :nth-child(4), :last-child  {
      display: none;
    }
  }
`;
