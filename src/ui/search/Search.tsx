import React, {BaseSyntheticEvent, FC} from 'react';
import styled from "styled-components";
import icon from '../../assets/icon/search_rounded.svg';

interface IProps {
    margin?: string;
    onChange: (event: BaseSyntheticEvent) => void;
}

export const Search: FC<IProps> = (props) => {
    return (
        <>
            <StyledSearchInput
                {...props}
                type="text"
                placeholder="Search..."
                defaultValue=''
            />
        </>
    );
};

const StyledSearchInput = styled.input`
  @media (${ props => props.theme.media.desktop }) {
    width: 364px;
    height: 40px;
    
    margin: ${ ( props: IProps ) => props.margin };
  }
  @media (${ props => props.theme.media.phone }) {
    width: 100%;
    height: 40px;
    
    margin-bottom: 16px;
  }
  color: ${ props => props.theme.colors.primary};
  background: url(${icon}) no-repeat bottom 12px right 12px, ${ props => props.theme.colors.basic};
  border-radius: 4px;
  border: 1px solid ${ props => props.theme.colors.lightestPrimary};
    
  font-size: 14px;
  line-height: 24px;
  padding: 8px 30px 8px 12px;
`;
