import React, {FC} from 'react';
import styled from "styled-components";

interface IProps {
    label: string,
    type: string,
    id: string,
    margin?: string,
    mobileWidth?: string,
    errorMessage?: string | undefined,
    onChange: (...event: any[]) => void,
    defaultValue: string | number,
}

export const Input: FC<IProps> = (props) => {
    const {
        label,
        type,
        id,
        errorMessage,
        defaultValue,
        onChange,
    } = props;
    return (
        <StyledContainer {...props}>
            <StyledLabel htmlFor={id}>{label}</StyledLabel>
            <StyledInput
                type={type}
                defaultValue={defaultValue}
                onChange={onChange}
            />
            { errorMessage && <StyledError>{errorMessage}</StyledError> }
        </StyledContainer>
    );
};

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: ${ ({margin}: IProps) => margin };
  width: 100%;
`;
const StyledInput = styled.input`
  @media (${ props => props.theme.media.desktop }) {
    font-size: 14px;
    line-height: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 17px;
    line-height: 24px;
  }
  width: 100%;
  height: 40px;
  color: ${ props => props.theme.colors.darkPrimary };
  background-color: ${ props => props.theme.colors.lightestPrimary1 };
  border-radius: 4px;

  font-weight: 500;
  padding: 8px 12px;

  border: 0;

  &:hover {
    background-color: ${ props => props.theme.colors.lightestPrimary };
  }
  &:focus {
    background-color: ${ props => props.theme.colors.lightestPrimary1 };
    box-shadow: 0 0 5px #D9D9D9;
  }
  &:disabled {
    color: ${ props => props.theme.colors.lightestPrimary };
  }
`;
const StyledLabel = styled.label`
  color: ${ props => props.theme.colors.primary };
  margin-bottom: 8px;
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
