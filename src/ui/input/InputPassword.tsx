import React, {FC, useCallback, useState} from 'react';
import styled from "styled-components";
import openEyeIcon from '../../assets/icon/eye_rounded.svg'
import closeEyeIcon from '../../assets/icon/close_eye_rounded.svg';

interface IProps {
    label: string,
    id: string,
    onChange: (...event: any[]) => void,
    errorMessage?: string | undefined,
}

export const InputPassword: FC<IProps> = (props) => {
    const [type, setType] = useState('password');
    const onClick = useCallback( () => {
        type === 'password'
            ? setType('text')
            : setType('password')
    }, [type, setType]);
    const { label, id, onChange, errorMessage } = props;
    return (
        <InputContainer>
            <StyledLabel htmlFor={id}>{label}</StyledLabel>
            <EyeContainer>
                <StyledInput
                    onChange={onChange}
                    type={type}
                />
                <EyeButton onClick={onClick}>
                    {type === 'password'
                        ? <img src={openEyeIcon} alt="see"/>
                        : <img src={closeEyeIcon} alt="hide"/>
                    }
                </EyeButton>
            </EyeContainer>
            { errorMessage && <StyledError>{errorMessage}</StyledError> }
        </InputContainer>
    );
};

const InputContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 24px;
`;
const StyledInput = styled.input`
  @media (${ props => props.theme.media.desktop }) {
    width: 366px;
  }
  @media (${ props => props.theme.media.phone }) {
    width: 327px;
  }
  height: 40px;
  color: ${ props => props.theme.colors.darkPrimary };
  background-color: ${ props => props.theme.colors.lightestPrimary1 };
  border-radius: 4px;

  font-size: 14px;
  line-height: 24px;
  font-weight: 500;
  padding: 8px 30px 8px 12px;
  
  border: 0;
  
  &:hover {
    background-color: ${ props => props.theme.colors.lightestPrimary };
  }
  &:focus {
    background-color: ${ props => props.theme.colors.lightestPrimary1 };
    box-shadow: 0 0 5px #D9D9D9;
  }
  &:disabled {
    color: ${ props => props.theme.colors.lightestPrimary };
  }
`;
const StyledLabel = styled.label`
  color: ${ props => props.theme.colors.primary };
  margin-bottom: 8px;
`;
const EyeContainer = styled.div`
  @media (${ props => props.theme.media.phone }) {
    width: 327px;
  }
    position: relative;
`;
const EyeButton = styled.div`
    cursor: pointer;
    position: absolute;
    right: 12px;
    bottom: 12px;
    background: transparent;
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
