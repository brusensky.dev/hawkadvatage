import React, {FC, useCallback} from 'react';
import styled from 'styled-components';
import logo from '../../assets/images/logo.svg';
import { User } from "../user/User";

interface IProps {
    showSidebar: (arg0: boolean) => void;
}

export const Header: FC<IProps> = ({showSidebar}) => {
    const onClick = useCallback( () => showSidebar(true), [showSidebar]);
    return (
        <StyledHeader>
            <ButtonToggle onClick={onClick}>
                <HamburgerMenuButton>
                    <ButtonLines/>
                    <ButtonLines/>
                    <ButtonLines/>
                </HamburgerMenuButton>
            </ButtonToggle>
            <Logo src={logo} alt='logo'/>
            <UserToggle>
                <User/>
            </UserToggle>
        </StyledHeader>
    );
}

const StyledHeader = styled.header`
  display: flex;
  grid-area: header;
  background: ${ props => props.theme.colors.basic };
  @media (${ props => props.theme.media.desktop }) {
    height: 80px;
    padding: 0 51px;
    justify-content: space-between;
    align-items: center;
    box-shadow: 0 1px 10px rgba(209, 209, 209, 0.5);
  }
  @media (${ props => props.theme.media.xLarge }) {
    z-index: 10;
  }
  @media (${ props => props.theme.media.medium }) {
    justify-content: center;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 62px;
    padding: 12px 14px;
  }
`;
const UserToggle = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    display: block;
  }
  @media (${ props => props.theme.media.medium }) {
    display: none;
  }
  @media (${ props => props.theme.media.phone }) {
    display: none;
  }
`;
const Logo = styled.img`
  @media (${ props => props.theme.media.desktop }) {
    height: 48px;
    width: 191px;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 34px;
    width: 137px;
  }
`;
const ButtonToggle = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    display: none;
  }
  @media (${ props => props.theme.media.xLarge }) {
    display: block;
  }
`;
const HamburgerMenuButton = styled.div`
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  @media (${ props => props.theme.media.xLarge }) {
    padding: 10px 6px;
    top: 19px;
    left: 22px;
    width: 40px;
    height: 40px;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 6px 3px;
    top: 19px;
    left: 12px;
    width: 24px;
    height: 24px;
  }
`;
const ButtonLines = styled.div`
  background: ${ props => props.theme.colors.primary };
  @media (${ props => props.theme.media.xLarge }) {
    width: 28px;
    height: 3px;
    border-radius: 2px;  
  }
  @media (${ props => props.theme.media.phone }) {
    width: 18px;
    height: 2px;
    border-radius: 1px;
  }
`;
