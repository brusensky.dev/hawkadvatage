import React, {FC, ReactText} from 'react';
import styled from "styled-components";

interface IErrorProps {
    picture: string;
    children: ReactText;
}
export const Empty: FC<IErrorProps> = ({picture, children}) => {
    return (
        <Flex>
            <StyledContainer>
                <Picture src={picture} alt='Empty'/>
                <StyledHeader>Empty here</StyledHeader>
                <StyledText>{ children }</StyledText>
            </StyledContainer>
        </Flex>
    );
};

const Flex = styled.div`
  display: flex;
  justify-content: center;
  align-self: center;
  @media (${ props => props.theme.media.desktop }) {
    width: 1140px;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 100vw;
  }
`;
const StyledContainer = styled.div`
    height: 570px;
    width: 556px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 48px 0;
    background: ${ props => props.theme.colors.basic };
    border-radius: 15px;
    @media (${ props => props.theme.media.phone }) {
      height: 434px;
      width: 100%;
      border-radius: 0;
      padding: 48px 0 19px 0;
    } 
`;
const Picture = styled.img`
  height: 320px;
  width: 320px;
  @media (${ props => props.theme.media.phone }) {
    height: 225px;
    width: 225px;
  }
`;
const StyledHeader = styled.div`
  margin: 48px 0 24px 0;
  font-weight: 800;
  font-size: 36px;
  line-height: 49px;
  color: ${props => props.theme.colors.lightestAccent};
  @media (${ props => props.theme.media.phone }) {
    font-size: 17px;
    line-height: 25px;
    margin: 48px 0 45px 0;
  }
`;
const StyledText = styled.div`
  font-size: 24px;
  line-height: 33px;
  color: ${props => props.theme.colors.primary};
  @media (${ props => props.theme.media.phone }) {
    font-size: 15px;
    line-height: 24px;
  }
`;
