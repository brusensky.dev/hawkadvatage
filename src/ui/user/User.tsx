import React, {FC} from 'react';
import styled from "styled-components";
import avatar from '../../assets/icon/profile_rounded.svg';
import {useSelector} from "react-redux";
import {selectCurrentUser} from "../../modules/authorization/authorizationSelector";

export const User: FC = () => {
    const currentUser = useSelector(selectCurrentUser)
    return (
        <StyledUser>
            <UserName>{currentUser.name}</UserName>
            <UserAvatar src={avatar} alt='user_avatar'/>
        </StyledUser>
    );
};

const StyledUser = styled.div`
  display: flex;
  align-items: center;
  line-height: 24px;
  color: ${ props => props.theme.colors.darkPrimary};
  @media (${ props => props.theme.media.desktop }) {
    font-size: 14px;
  }
  @media (${ props => props.theme.media.medium }) {
    padding: 20px;
    font-size: 15px;
  }
`;
const UserName = styled.span`
  @media (${ props => props.theme.media.medium }) {
    order: 2;
  }
`;
const UserAvatar = styled.img`
  @media (${ props => props.theme.media.desktop }) {
    margin: 0 0 0 16px;
  }
  @media (${ props => props.theme.media.medium }) {
    order: 1;
    margin: 0 12px 0 0;
  }
  height: 40px;
  width: 40px;
`;
