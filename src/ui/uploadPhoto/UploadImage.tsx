import React, {BaseSyntheticEvent, FC, useCallback, useEffect, useState} from 'react';
import styled from "styled-components";
import addPhotoIcon from '../../assets/icon/add_photo_icon.svg'

interface IProps {
    id: string,
    getImageFile: (imageFile: File) => void,
    preloadedImageUrl?: string,
    onChange: (...event: any) => void,
}

export const UploadImage: FC<IProps> = (props) => {
    const {
        onChange,
        getImageFile,
        preloadedImageUrl,
        id,
    } = props;
    const [file, setFile] = useState<string>();
    useEffect( () => {
        if ( preloadedImageUrl )
            setFile(preloadedImageUrl);
    }, [preloadedImageUrl]);
    const onFileSelected = useCallback( (event: BaseSyntheticEvent) => {
        const uploadedFile = event.target.files[0];
        setFile( URL.createObjectURL(uploadedFile) );
        getImageFile(uploadedFile);
        onChange(event);
    },[setFile, onChange, getImageFile]);
    return (
        <StyledContainer>
            <label htmlFor={id}>
                <StyledBackground>
                    <UploadedPhoto src={file}/>
                    <HiddenInput
                        type='file'
                        id={id}
                        onChange={onFileSelected}
                    />
                    <Icon src={addPhotoIcon} alt="#"/>
                </StyledBackground>
            </label>
        </StyledContainer>
    );
};

const StyledContainer = styled.div`
  border-radius: 10px;
  overflow: hidden;
  position: relative;
  @media (${ props => props.theme.media.desktop }) {
    width: 336px;
    height: 261px;
  }
  @media (${ props => props.theme.media.phone }) {
    width: 185px;
    height: 144px;
  }
`;
const StyledBackground = styled.div`
  background: rgba(${props => props.theme.colors.lightPrimaryRgb}, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  &:hover {
    cursor: pointer;
  }
`;
const Icon = styled.img`
  opacity: 0.7;
  @media (${ props => props.theme.media.desktop }) {
    width: 74px;
    height: 75px;
  }
  @media (${ props => props.theme.media.phone }) {
    width: 41px;
    height: 40px;
  }
`;
const UploadedPhoto = styled.img`
  width: 100%;
  height: 100%;
  opacity: 0.7;
  position: absolute;
`;
const HiddenInput = styled.input`
    display: none;
`;
