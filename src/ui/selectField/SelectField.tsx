import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import Select from "react-select";
import { IOption } from "../pagination/Pagination";
import { Control, Controller, FieldError } from "react-hook-form";
import {PlayerFormData} from "../../pages/addPlayer/AddPlayer";

interface IProps {
    control: Control<PlayerFormData>
    name: "number" | "name" | "height" | "position" | "team" | "birthday" | "weight" | "avatarUrl",
    label: string,
    options: IOption[],
    errors:  FieldError | undefined,
}
export const SelectField: FC<IProps> = (props) => {
    const {
        options,
        control,
        label,
        errors,
        name
    } = props;
    const getDefaultValue = useCallback( (data:  IOption[], value: string|number|undefined|null) => {
        return data.filter( ( option) => option.value === value?.toString() )[0];
    }, []);
    return (
        <Container>
            <StyledLabel>{label}</StyledLabel>
            <Controller
                control={control}
                name={name}
                rules={{ required: 'Required.' }}
                render={({ field: { onChange, value, ref } }) => (
                    <Select
                        options={options}
                        defaultValue={getDefaultValue( options, value )}
                        inputRef={ref}
                        onChange={(e) => onChange(e?.value)}
                        styles={Styles}
                    />
                )}
            />
            { errors && <StyledError>{errors.message}</StyledError> }
        </Container>
    );
};
const Styles = {
    container: (styles: any) => ({
        ...styles,
        marginTop: '8px',
    }),
    control: (styles: any) => ({
        ...styles,
        height: '40px',
        borderRadius: '4px',
        border: 0,
        background: '#f6f6f6',
    }),
    singleValue: (styles: any) => ({
        ...styles,
        color: '#303030',
        fontSize: '14px',
        lineHeight: '24px',
    }),
};
const Container = styled.div`
    margin: 0 0 24px 0;
`;
const StyledLabel = styled.label`
  color: ${ props => props.theme.colors.primary };
  margin-bottom: 8px;
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
