import React, {FC} from 'react';
import styled, {css} from "styled-components";

interface IProps {
    cancel?: boolean,
    disabled?: boolean,
    type?: 'submit' | 'reset' | 'button';
    onClick?: () => void,
}

export const Button: FC<IProps> = (props)  => {
    return (
        <StyledButton {...props}>
            {props.children}
        </StyledButton>
    );
};

const StyledButton = styled.button`
  @media (${ props => props.theme.media.desktop }) {
    width: 171px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 100%;
  }
  color: ${ props => props.theme.colors.basic };
  background-color: ${ props => props.theme.colors.accent };
  
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;

  height: 40px;
  padding: 8px 24px;
  border-radius: 4px;
  
  &:hover {
    background-color: ${ props => props.theme.colors.lightAccent };
  }
  &:active {
    background-color: ${ props => props.theme.colors.darkAccent };
  }
  &:disabled {
    color: ${ props => props.theme.colors.lightestPrimary };
    background-color: ${ props => props.theme.colors.lightestPrimary1 };
  }
  
  ${ ( props: IProps ) => props.cancel && css`
    color: ${ props => props.theme.colors.lightPrimary };
    background-color: ${ props => props.theme.colors.basic };
    border: 1px solid ${ props => props.theme.colors.lightPrimary };

    &:hover {
      background-color: ${ props => props.theme.colors.lightestPrimary };
      border: 1px solid ${ props => props.theme.colors.lightPrimary };
    }
    &:active {
      color: ${ props => props.theme.colors.primary };
      background-color: ${ props => props.theme.colors.lightPrimary };
      border: 1px solid ${ props => props.theme.colors.primary };
    }
    &:disabled {
      color: ${ props => props.theme.colors.lightestPrimary };
      background-color: ${ props => props.theme.colors.lightestPrimary1 };
      border: 1px solid ${ props => props.theme.colors.lightPrimary };
    }
  `};
`;
