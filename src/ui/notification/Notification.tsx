import React, {FC} from 'react';
import styled from "styled-components";

interface INotificationProps {
    children: string,
    top?: string,
    bottom?: string,
    left?: string,
    right?: string,
}
export const Notification: FC<INotificationProps> = (props) => {
    return (
        <StyledNotification {...props}>
            {props.children}
        </StyledNotification>
    );
};

const StyledNotification = styled.div`
  position: absolute;
  
  font-size: 16px;
  line-height: 24px;

  color: ${ props => props.theme.colors.basic };
  background-color: ${ props => props.theme.colors.lightAccent };
  
  border-radius: 4px;
  padding: 8px 16px;
  @media (${ props => props.theme.media.desktop }) {
    top: 36px;
    right: 36px;
  }
  @media (${ props => props.theme.media.phone }) {
    top: 24px;
    right: 24px;
    left: 24px;
  }
`;
