import React, {FC} from 'react';
import { Control, Controller } from "react-hook-form";
import ReactDatePicker from "react-datepicker";
import styled from "styled-components";
import "react-datepicker/dist/react-datepicker.css";
import {PlayerFormData} from "../../pages/addPlayer/AddPlayer";

interface IProps {
    control: Control<PlayerFormData>,
    errorsMessage:  string | undefined,
}

export const InputDate: FC<IProps> = ({control, errorsMessage}) => {
    return (
        <Container>
            <StyledLabel>Birthday</StyledLabel>
            <Controller
                control={control}
                name="birthday"
                rules={ {required: 'Required'} }
                render={({ field }) => (
                    <ReactDatePicker
                        className="StyleInputDate"
                        placeholderText="Select date"
                        onChange={(e) => field.onChange(e)}
                        selected={ field.value ? new Date(field.value) : undefined }
                    />
                )}
            />
            { errorsMessage && <StyledError>{errorsMessage}</StyledError> }
        </Container>
    );
};
const Container = styled.div`
    margin: 0 0 24px 0;
  display: flex;
  flex-direction: column;
  & .StyleInputDate {
    @media (${ props => props.theme.media.desktop }) {
      width: 171px;
      font-size: 14px;
      line-height: 24px;
    }
    @media (${ props => props.theme.media.phone }) {
      width: 152px;
      font-size: 17px;
      line-height: 24px;
    }
    height: 40px;
    color: ${ props => props.theme.colors.darkPrimary };
    background-color: ${ props => props.theme.colors.lightestPrimary1 };
    border-radius: 4px;
    font-weight: 500;
    padding: 8px 12px;
    border: 0;
  }
`;
const StyledLabel = styled.label`
  color: ${ props => props.theme.colors.primary };
  margin-bottom: 8px;
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
