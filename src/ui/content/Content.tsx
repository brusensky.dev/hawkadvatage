import React, { FC } from 'react';
import styled from "styled-components";

export const Content: FC = ( props ) => {
    return (
            <StyledContent>
                <Container>
                    { props.children }
                </Container>
            </StyledContent>
    );
};

const StyledContent = styled.main`
  grid-area: main;
  background: ${props => props.theme.colors.lightestPrimary1};
  display: flex;
  justify-content: center;
  @media (${ props => props.theme.media.desktop }) {
    padding: 32px 24px;
  }
  @media (${ props => props.theme.media.medium }) {
    padding: 32px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 16px 0;
  }
`;
const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  @media (${ props => props.theme.media.desktop }) {
    max-width: 1140px;
    align-items: center;
  }
  @media (${ props => props.theme.media.large }) {
    max-width: 752px;
    align-items: center;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 100%;
  }
`;
