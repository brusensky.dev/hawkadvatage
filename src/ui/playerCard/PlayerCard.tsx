import React, {FC, useCallback} from 'react';
import styled from "styled-components";
import {useHistory} from "react-router-dom";

interface IPlayerCardProps {
    name: string,
    number: number,
    team: string,
    imageUrl: string,
    id: number,
}
export const PlayerCard: FC<IPlayerCardProps> = (props) => {
    const history = useHistory();
    const onClick = useCallback(() => history.push(`players/detail/${props.id}`), [history, props.id]);
    return (
        <StyledCard onClick={onClick}>
            <CardImageContainer>
                <PlayerPhoto>
                    <img src={props.imageUrl} alt={props.name}/>
                </PlayerPhoto>
            </CardImageContainer>
            <CardPlayerNameContainer>
                <CardPlayerName>
                    {props.name}
                    &nbsp;
                    <span>#{props.number}</span>
                </CardPlayerName>
                <CardPlayerTeam>{props.team}</CardPlayerTeam>
            </CardPlayerNameContainer>
        </StyledCard>
    );
};
const StyledCard = styled.div`
  border-radius: 4px;
  overflow: hidden;
  &:hover {
    cursor: pointer;
  }
  @media (${ props => props.theme.media.desktop }) {
    height: 380px;
    width: 364px;
    margin: 0 24px 24px 0;
    &:nth-child(3n) {
      margin: 0 0 24px 0;
    }
  }
  @media (${ props => props.theme.media.large }) {
    margin: 0 0 24px 0;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 48%;
    height: 30%;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 180px;
    width: 48%;
    margin-bottom: 12px;
  }
`;
const CardImageContainer = styled.div`
  background: linear-gradient(121.57deg, #707070 1.62%, #393939 81.02%);
  @media (${ props => props.theme.media.desktop }) {
    padding: 73px 0 0 0;
    display: flex;
    justify-content: center;
    align-items: flex-end;
  }
  @media (${ props => props.theme.media.medium }) {
    padding: 13px 0 0 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 104px;
    display: flex;
    justify-content: center;
    align-items: flex-end;
  }
`;
const PlayerPhoto = styled.div`
  overflow: hidden;
  display: flex;
  justify-content: center;
  @media (${ props => props.theme.media.desktop }) {
    height: 207px;
    width: 274px;
    & img {
      height: 207px;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    height: 93px;
    width: 122px;
    & img {
      height: 93px;
    }
  }
`;
const CardPlayerNameContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.colors.darkPrimary};
  @media (${ props => props.theme.media.desktop }) {
    height: 100px;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 76px;
  }
`;
const CardPlayerName = styled.div`
  color: ${props => props.theme.colors.basic};
  & span {
    color: ${props => props.theme.colors.lightAccent};
  }
  @media (${ props => props.theme.media.desktop }) {
    font-size: 18px;
    line-height: 25px;
    margin-bottom: 12px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 6px;
  }
`;
const CardPlayerTeam = styled.div`
  color: ${props => props.theme.colors.lightPrimary};
  @media (${ props => props.theme.media.desktop }) {
    font-size: 14px;
    line-height: 24px;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 13px;
    line-height: 18px;
  }
`;
