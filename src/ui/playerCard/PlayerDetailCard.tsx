import React, {FC, useCallback, useEffect} from 'react';
import styled from 'styled-components';
import { IPlayer } from "../../api/dto/IPlayer";
import deleteIcon from '../../assets/icon/delete_rounded.svg';
import editIcon from '../../assets/icon/create.svg';
import { Link, useHistory } from "react-router-dom";
import { getAge, getTeamName } from "../../pages/utils";
import { useAppDispatch } from "../../core/redux/hooks";
import {fetchTeams} from "../../modules/teams/teamsThunk";
import {deletePlayerById} from "../../modules/players/playersThunk";
import {selectTeams} from "../../modules/teams/teamsSelector";
import {useSelector} from "react-redux";

interface IProps {
    player: IPlayer,
}

export const PlayerDetailCard: FC<IProps> = ({player}) => {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(fetchTeams({}));
    }, [dispatch]);
    const teams = useSelector(selectTeams);
    const history = useHistory();
    const onSuccessDeletion = useCallback( () => {
        history.push('/players');
    }, [history]);
    const onClickDelete = useCallback(() => {
        dispatch(deletePlayerById({
            id: player.id,
            onSuccess: onSuccessDeletion,
        }));
    }, [dispatch, player.id, onSuccessDeletion]);
    const onClickEdit = useCallback(() => history.push(`/players/edit/${player.id}`), [history, player.id]);
    return (
        <StyledCard>
            <CardHeader>
                <PathBlock>
                    <Link to={'/players'}>
                        <PathText>Players</PathText>
                    </Link>
                    &nbsp;/&nbsp;
                    <PathText>{player.name}</PathText>
                </PathBlock>
                <ActionsBlock>
                    <img
                        src={editIcon}
                        alt={'edit'}
                        onClick={onClickEdit}
                    />
                    <img
                        src={deleteIcon}
                        alt={'delete'}
                        onClick={onClickDelete}
                    />
                </ActionsBlock>
            </CardHeader>
            <CardContent>
                <PlayerPhoto>
                    <img
                        src={player.avatarUrl || ''}
                        alt={player.name}
                    />
                </PlayerPhoto>
                <PlayerInfoContainer>
                    <PlayerInfoName>
                        {player.name}
                        <span className="number">#{player.number}</span>
                    </PlayerInfoName>
                    <PlayerInfoList>
                        <PlayerInfoItem>
                            <div className="title">Position</div>
                            <div className="value">{player.position}</div>
                        </PlayerInfoItem>
                        <PlayerInfoItem>
                            <div className="title">Team</div>
                            <div className="value">{getTeamName(player.team, teams)}</div>
                        </PlayerInfoItem>
                        <PlayerInfoItem>
                            <div className="title">Height</div>
                            <div className="value">{`${player.height} cm`}</div>
                        </PlayerInfoItem>
                        <PlayerInfoItem>
                            <div className="title">Weight</div>
                            <div className="value">{`${player.weight} kg`}</div>
                        </PlayerInfoItem>
                        <PlayerInfoItem>
                            <div className="title">Age</div>
                            <div className="value">
                                { player.birthday
                                    ? getAge(player.birthday)
                                    : '-'
                                }
                            </div>
                        </PlayerInfoItem>
                    </PlayerInfoList>
                </PlayerInfoContainer>
            </CardContent>
        </StyledCard>
    );
};
const StyledCard = styled.div`
    overflow: hidden;
  @media (${ props => props.theme.media.desktop }) {
    width: 1140px;
    border-radius: 10px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 752px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
    width: 100vw;
  }
`;
const CardHeader = styled.div`
    height: 70px;
    border: 0.5px solid ${ props => props.theme.colors.lightPrimary };
    display: flex;
    justify-content: space-between;
  @media (${ props => props.theme.media.desktop }) {
    height: 70px;
    border-radius: 10px 10px 0 0;
    padding: 24px 32px 0 32px;
    line-height: 24px;
    font-size: 14px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 48px;
    padding: 12px 16px;
    line-height: 18px;
    font-size: 13px;
  }
`;
const PathBlock = styled.div``;
const PathText = styled.span`
    color: ${ props => props.theme.colors.accent };
`;
const ActionsBlock = styled.div`
    width: 64px;
    display: flex;
    justify-content: space-between;
    & img {
      height: 24px;
      width: 24px;
      cursor: pointer;
    }
`;
const CardContent = styled.div`
    color: ${ props => props.theme.colors.basic };
    background: linear-gradient(276.45deg, #707070 0%, #393939 100.28%);
    display: flex;
  @media (${ props => props.theme.media.desktop }) {
    padding: 66px 0 0 1px;
    height: 525px;
  }
  @media (${ props => props.theme.media.large }) {
    height: auto;
    flex-direction: column;
    align-items: center;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 48px 0;
    flex-direction: column;
    align-items: center;
  }
`;
const PlayerPhoto = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    width: 531px;
    margin-top: 60px;
    & img {
      width: 531px;
    }
  }
  @media (${ props => props.theme.media.large }) {
    margin: auto 0 60px 0;
  }
  @media (${ props => props.theme.media.phone }) {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 112px;
    width: 143px;
    margin-bottom: 48px;
    & img {
      height: 112px;
      width: 143px;
    }
  }
`;
const PlayerInfoContainer = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    width: 600px;
    padding: 0 0 0 56px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 80%;
  }
  @media (${ props => props.theme.media.medium }) {
    width: 100vw;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
const PlayerInfoName = styled.div`
    font-weight: 800;
  & .number {
    color: ${ props => props.theme.colors.accent };
  }
  @media (${ props => props.theme.media.desktop }) {
    font-size: 36px;
    line-height: 49px;
  }
  @media (${ props => props.theme.media.large }) {
    text-align: center;
  }
  @media (${ props => props.theme.media.phone }) {
    font-size: 17px;
    line-height: 25px;
  }
`;
const PlayerInfoList = styled.div`
    display: flex;
    padding: 40px 0 0 0;
  @media (${ props => props.theme.media.desktop }) {
    flex-wrap: wrap;
  }
  @media (${ props => props.theme.media.medium }) {
    flex-direction: column;
  }
`;
const PlayerInfoItem = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    flex: 50%;
    margin-bottom: 54px;
    & .title {
      font-weight: 800;
      font-size: 24px;
      line-height: 33px;
      margin-bottom: 8px;
    }
    & .value {
      font-weight: 500;
      font-size: 18px;
      line-height: 25px;
    }
  }
  @media (${ props => props.theme.media.large }) {
    & .title {
      text-align: center;
    }
    & .value {
      text-align: center;
    }
  }
  @media (${ props => props.theme.media.medium }) {
    display: flex;
    flex: 0;
    flex-direction: column;
    align-items: center;
    margin-bottom: 32px;
  }
  @media (${ props => props.theme.media.phone }) {
    display: flex;
    flex-direction: column;
    align-items: center;
    & .title {
      font-weight: 800;
      font-size: 17px;
      line-height: 25px;
      margin-bottom: 8px;
    }
    & .value {
      font-weight: 500;
      font-size: 15px;
      line-height: 25px;
    }
  }
`;
