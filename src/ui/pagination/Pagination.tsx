import React, {FC, useCallback} from 'react';
import ReactPaginate from 'react-paginate';
import styled from "styled-components";
import { leftArrow, rightArrow } from './arrows';
import { theme as styledTheme } from "../../assets/theme/theme";
import Select from "react-select";

export interface IOption {
    value: number | string,
    label: string,
}

const options: IOption[] = [
    { value: 6, label: '6' },
    { value: 12, label: '12' },
    { value: 24, label: '24' },
];
interface IPaginationProps {
    setPage: (page: number) => void,
    setPageSize: (pageSize: number) => void,
    itemsCount: number;
    pageSize: number;
}
export const Pagination: FC<IPaginationProps> = (props) => {
    const {
        pageSize,
        itemsCount,
        setPageSize,
        setPage
    } = props;
    const pageCount = Math.ceil(itemsCount / pageSize );
    const onPageChange = useCallback( (e: {selected: number}) => {
        setPage(e.selected + 1);
    }, [setPage]);
    const onPageSizeChange = useCallback( (state: IOption) => setPageSize(+state.value), [setPageSize]);
    return (
        <StyledPaginationContainer>
            <ReactPaginate
                previousLabel={leftArrow}
                nextLabel={rightArrow}
                breakLabel={'...'}
                breakClassName={'page'}
                pageCount={pageCount}
                marginPagesDisplayed={1}
                pageRangeDisplayed={3}
                containerClassName={'pagination'}
                pageClassName={'page'}
                pageLinkClassName={'pageLink'}
                activeClassName={'active'}
                previousClassName={'page'}
                nextClassName={'page'}
                previousLinkClassName={'pageLink'}
                nextLinkClassName={'pageLink'}
                onPageChange={onPageChange}
            />
            <StyledSelect
                menuPlacement={'top'}
                defaultValue={options[0]}
                classNamePrefix={'Select'}
                options={options}
                onChange={onPageSizeChange}
                theme = {(theme: any) => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                        ...theme.colors,
                        primary25: `${ styledTheme.colors.lightestPrimary1 }`,
                        primary50: `${ styledTheme.colors.lightestAccent }`,
                        primary: `${ styledTheme.colors.darkAccent }`,
                    },
                })}
            />
        </StyledPaginationContainer>
    );
};

const StyledPaginationContainer = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  & .page {
    color: ${props => props.theme.colors.primary};
    cursor: pointer;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
  & .active {
    background: ${props => props.theme.colors.accent};
    color: ${props => props.theme.colors.basic};
    border-radius: 4px;
  }
  & .pageLink {
    width: 100%;
    text-align: center;
  }
  @media (${ props => props.theme.media.desktop }) {
    & .page {
      height: 40px;
      width: 40px;
      line-height: 25px;
      font-size: 18px;
      margin: 0 17px 0 0;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 0 12px;
    & .page {
      height: 28px;
      width: 28px;
      line-height: 24px;
      font-size: 15px;
      margin: 0 10px 0 0;
    }
  }
`;
const StyledSelect = styled(Select)`
  & .Select__control {
    width: 88px;
    height: 40px;
    border-radius: 4px;
    @media ( ${ styledTheme.media.phone } ) {
      height: 28px;
      width: 70px;
    }
  }
`;
