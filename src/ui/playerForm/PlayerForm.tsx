import React, {FC, FormEventHandler} from 'react';
import {Link} from "react-router-dom";
import {Input} from "../input/Input";
import {SelectField} from "../selectField/SelectField";
import {Button} from "../button/Button";
import styled from "styled-components";
import {IOption} from "../pagination/Pagination";
import {IPlayer} from "../../api/dto/IPlayer";
import {InputDate} from "../inputDate/InputDate";
import {Controller, Control} from "react-hook-form";
import {UploadImage} from "../uploadPhoto/UploadImage";
import {PlayerFormData} from "../../pages/addPlayer/AddPlayer";
import {FieldErrors} from "react-hook-form/dist/types/errors";


interface IProps {
    onSubmit: FormEventHandler<HTMLFormElement>,
    onClickCancel: () => void,
    defaultValues?: IPlayer,
    errors: FieldErrors<PlayerFormData>,
    control: Control<PlayerFormData>,
    positions: IOption[],
    teams: IOption[],
    getImageFile: (imageFile: any) => void,
}

export const PlayerForm: FC<IProps> = (props) => {
    const {
        onSubmit,
        onClickCancel,
        defaultValues,
        errors,
        control,
        positions,
        teams,
        getImageFile,
    } = props;
    return (
        <StyledCard>
            <CardHeader>
                <div className="path">
                    <Link to={'/players'}><PathText>Players</PathText></Link>
                    &nbsp;/&nbsp;
                    { defaultValues
                        ? <PathText>Edit player</PathText>
                        : <PathText>Add player</PathText>
                    }
                </div>
            </CardHeader>
            <CardContent onSubmit={onSubmit}>
                <ImageContainer>
                    <Controller
                        control={control}
                        name='avatarUrl'
                        rules={ { required: 'Required'} }
                        render={ ({field: { name, onChange}}) => (
                            <UploadImage
                                onChange={onChange}
                                id={name}
                                getImageFile={getImageFile}
                                preloadedImageUrl={ defaultValues?.avatarUrl || undefined }
                            />
                        )}
                    />
                    { errors.avatarUrl && <StyledError>{errors.avatarUrl.message}</StyledError> }
                </ImageContainer>
                <Info>
                    <Controller
                        control={control}
                        name={'name'}
                        rules={ {required: 'Required'} }
                        render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                            <Input
                                label='Name'
                                id={name}
                                type='text'
                                defaultValue={value}
                                onChange={onChange}
                                margin='0 0 24px 0'
                                errorMessage={error?.message}
                            />
                        )}
                    />
                    <SelectField
                        control={control}
                        name='position'
                        options={positions}
                        errors={errors.position}
                        label='Position'
                    />
                    <SelectField
                        control={control}
                        name='team'
                        options={teams}
                        errors={errors.team}
                        label='Team'
                    />
                    <Container>
                        <Controller
                            control={control}
                            name={'height'}
                            rules={ {required: 'Required'} }
                            render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                                <Input
                                    label='Height (cm)'
                                    id={name}
                                    type='number'
                                    defaultValue={value}
                                    onChange={onChange}
                                    margin='0 0 24px 0'
                                    errorMessage={error?.message}
                                />
                            )}
                        />
                        <Controller
                            control={control}
                            name={'weight'}
                            rules={ {required: 'Required'} }
                            render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                                <Input
                                    label='Weight (kg)'
                                    id={name}
                                    type='number'
                                    defaultValue={value}
                                    onChange={onChange}
                                    margin='0 0 24px 0'
                                    errorMessage={error?.message}
                                />
                            )}
                        />
                    </Container>
                    <Container>
                        <InputDate
                            control={control}
                            errorsMessage={ errors.birthday?.message }
                        />
                        <Controller
                            control={control}
                            name={'number'}
                            rules={ {required: 'Required'} }
                            render={ ({field: {value, onChange, name}, fieldState: {error}}) => (
                                <Input
                                    label='Number'
                                    id={name}
                                    type='number'
                                    defaultValue={value}
                                    onChange={onChange}
                                    margin='0 0 24px 0'
                                    errorMessage={error?.message}
                                />
                            )}
                        />
                    </Container>
                    <Container>
                        <Button
                            type='reset'
                            cancel
                            onClick={onClickCancel}
                        >
                            Cancel
                        </Button>
                        <Button type='submit'>Save</Button>
                    </Container>
                </Info>
            </CardContent>
        </StyledCard>
    );
};
const StyledCard = styled.div`
  overflow: hidden;
  background: ${props => props.theme.colors.basic};
  @media (${ props => props.theme.media.desktop }) {
    width: 1140px;
    border-radius: 10px;
  }
  @media (${ props => props.theme.media.large }) {
    width: 752px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
    width: 100vw;
  }
`;
const CardHeader = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    height: 70px;
    border-radius: 10px 10px 0 0;
    padding: 24px 32px 0 32px;
    line-height: 24px;
    font-size: 14px;
  }
  @media (${ props => props.theme.media.medium }) {
    border-radius: 0;
  }
  @media (${ props => props.theme.media.phone }) {
    height: 48px;
    padding: 16px;
    line-height: 18px;
    font-size: 13px;
  }
`;
const PathText = styled.span`
  color: ${props => props.theme.colors.accent};
`;
const CardContent = styled.form`
  display: flex;
  @media (${ props => props.theme.media.desktop }) {
    padding: 30px 0 30px 0;
  }
  @media (${ props => props.theme.media.large }) {
    height: auto;
    flex-direction: column;
    align-items: center;
  }
  @media (${ props => props.theme.media.phone }) {
    flex-direction: column;
    padding-bottom: 48px;
  }
`;
const ImageContainer = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    padding: 0 73px;
  }
  @media (${ props => props.theme.media.large }) {
    margin-bottom: 30px;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 48px 95px;
  }
`;
const Info = styled.div`
  @media (${ props => props.theme.media.desktop }) {
    margin-left: 63px;
  }
  @media (${ props => props.theme.media.large }) {
    margin-left: 0;
  }
  @media (${ props => props.theme.media.phone }) {
    padding: 0 24px;
  }
`;
const Container = styled.div`
  display: flex;
  justify-content: space-between;
  @media (${ props => props.theme.media.large }) {
    width: 366px;
    & button, & > div {
      width: 171px;
    }
  }
  @media (${ props => props.theme.media.phone }) {
    & button & > div {
      width: 152px;
    }
  }
`;
const StyledError = styled.div`
  line-height: 24px;
  font-size: 12px;
  color: ${ props => props.theme.colors.lightestAccent };
`;
