export interface IPlayer extends INewPlayerDTO{
    id: number,
}
export interface INewPlayerDTO {
    name: string,
    number: number,
    position: string,
    team: number,
    birthday: string,
    height: number,
    weight: number,
    avatarUrl: string | null,
}
export interface PlayerDtoPageResult {
    data: IPlayer[],
    count: number,
    page: number,
    size: number,
}
