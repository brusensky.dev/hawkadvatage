export interface IDetailProblem {
    type: string | null,
    title: string | null,
    status:	number | null,
    traceId: string | null,
}
