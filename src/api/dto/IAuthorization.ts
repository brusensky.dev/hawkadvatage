export interface IAuthorization {
    login: string,
    password: string,
}

export interface IRegistration extends IAuthorization {
    userName: string;
}
export interface ILoginResult {
    name: string | null,
    avatarUrl: string | null,
    token: string | null,
}
