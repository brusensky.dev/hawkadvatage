export interface ITeam {
    name: string,
    foundationYear:	number,
    division: string,
    conference:	string,
    imageUrl: string | null,
    id: number,
}
export interface INewTeamDTO {
    name: string,
    foundationYear:	number,
    division: string,
    conference:	string,
    imageUrl: string | null,
}

export interface TeamDtoPageResult {
    data: ITeam[],
    count: number,
    page: number,
    size: number,
}
