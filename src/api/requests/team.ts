import {baseHttpRequest, baseUrl, getRequestUrl} from "../baseRequests";
import { INewTeamDTO, ITeam } from "../dto/ITeam";

export const getTeams = (name?: string, page?: number, pageSize?: number) => {
    const params = {
        name,
        page,
        pageSize
    }
    const requestUrl = getRequestUrl(params, `${baseUrl}/api/Team/GetTeams`);
    return baseHttpRequest(requestUrl, {});
};
export const getTeam = (id: number) => {
    const requestUrl = getRequestUrl({id}, `${baseUrl}/api/Team/Get`);
    return baseHttpRequest(requestUrl, {});
};
export const addTeam = (data: INewTeamDTO) => {
    return baseHttpRequest(`${baseUrl}/api/Team/Add`, {
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify(data),
    });
};
export const updateTeam = (data: ITeam) => {
    return baseHttpRequest(`${baseUrl}/api/Team/Update`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
};
export const deleteTeam = (id: number) => {
    const requestUrl = getRequestUrl({id}, `${baseUrl}/api/Team/Delete`)
    return baseHttpRequest(requestUrl,{
        method: 'DELETE',
    })
};
