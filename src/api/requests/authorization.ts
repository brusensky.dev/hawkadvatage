import {IAuthorization, IRegistration} from "../dto/IAuthorization";
import {baseHttpRequest, baseUrl} from "../baseRequests";


export const login = (data: IAuthorization) => {
    return baseHttpRequest(`${baseUrl}/api/Auth/SignIn`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    });
};
export const signUp = (data: IRegistration) => {
    return  baseHttpRequest(`${baseUrl}/api/Auth/SignUp`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    });
};
