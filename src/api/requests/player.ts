import {baseHttpRequest, baseUrl, getRequestUrl} from "../baseRequests";
import {INewPlayerDTO, IPlayer} from "../dto/IPlayer";

export const getPositions = () => {
    return baseHttpRequest(`${baseUrl}/api/Player/GetPositions`,{});
};
export const getPlayers = (name?: string, teamIds?: number[], page?: number, pageSize?: number) => {
    const params = {
        name,
        teamIds,
        page,
        pageSize
    }
    const requestUrl = getRequestUrl(params, `${baseUrl}/api/Player/GetPlayers`);
    return baseHttpRequest(requestUrl, {});
};
export const getPlayer = (id: number) => {
    const requestUrl = getRequestUrl({id}, `${baseUrl}/api/Player/Get`);
    return baseHttpRequest(requestUrl, {});
};
export const addPlayer = (data: INewPlayerDTO) => {
    return baseHttpRequest(`${baseUrl}/api/Player/Add`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
};
export const updatePlayer = (data: IPlayer) => {
    return baseHttpRequest(`${baseUrl}/api/Player/Update`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
};
export const deletePlayer = (id: number) => {
    const requestUrl = getRequestUrl({id}, `${baseUrl}/api/Player/Delete`)
    return baseHttpRequest(requestUrl,{
        method: 'DELETE',
    });
};
