import {baseHttpRequest, baseUrl} from "../baseRequests";

export const saveImage = (file: File) => {
    const formData = new FormData();
    formData.append('file', file);
    return baseHttpRequest(`${baseUrl}/api/Image/SaveImage`,{
        method: 'POST',
        body: formData,
    });
};
export const deleteImage = (fileName: string): void => {
    baseHttpRequest(`${baseUrl}/api/Image/DeleteImage`,{
        method: 'DELETE',
        body: JSON.stringify(fileName),
    }).catch(reason => console.log(reason));
};
