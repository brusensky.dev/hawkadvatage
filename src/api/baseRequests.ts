export const baseUrl = '';

export async function baseHttpRequest(requestUrl: string, options: RequestInit) {
    const token = localStorage.getItem('token');
    if (token)
        options.headers = {
            ...options.headers,
            'Authorization': `Bearer ${token}`
        }
    return await fetch(requestUrl, {
        ...options,
    }).then(response => response.json())
}

export function getRequestUrl(params: Record<string, string | number | number[] | undefined>, defaultUrl: string) {
    return Object.keys(params)
        .reduce( ( acc: string, curr: string ) => {
            let arr = params[curr];
            if ( Array.isArray(arr) ) {
                return `${acc}${getRequestUrlFromArray( arr, curr )}`;
            }
            if (params[curr]) {
                return `${acc}${curr}=${params[curr]}&`;
            }
            else
                return `${acc}`;
        }, `${defaultUrl}?`);
}
function getRequestUrlFromArray(arr: number[], key: string) {
    return arr.reduce( (acc: string, curr: number) => `${acc}${key}=${curr}&`, '');
}
